Projeto - Portal de Crise Front End

# Describe project

# INFORMATIVO

Deve-se utilizar essa opção, quando a mensagem for apenas um informativo, ou uma situação que não haja impacto referente ao ambiente.

# DEGRADAÇÃO

Deve-se utilizar essa opção quando necessário informar uma indisponibilidade ou um problema que afete um ambientede forma parcial, onde haja impacto direto ou indireto ao funcionamento do ambiente core ou corporativo.

# CRISE DE TI

Deve-se utilizar essa opção quando necessário informar uma indisponibilidade total em um ambiente, onde haja impacto ao funcionamento do ambiente core ou corporativo, neste caso pode ou não decorrer de uma inicial degradação.

## Aviso

Para todos os casos, informar sempre o número do evento registrado na central de serviço.

# Exemplos de menssagens

📣 [INFORMATIVO] EV12345

-   <b>Serviço</b>: PCR PRD
-   <b>Sintoma</b>: Falha na entrega de arquivos via C:D para o Bradesco desde 12:01, impactando entrega dos arquivos ADDA110 (Consulta de Boletos).
-   <b>Status</b>: Acionada produção e notificado participante.

📣 [DEGRADAÇÃO] EV12345

-   <b>Serviço</b>: SLC PRD
-   <b>Sintoma</b>: Lentidão no inject de arquivos via CFT desde 12:01. Impacto em análise.
-   <b>Status</b>: N2 Middleware atuando. Conferência: 4502-2772 Sala: 709030#.

📣 [CRISE DE TI] EV12345

-   <b>Serviço</b>: Portal CQL PRD
-   <b>Sintoma</b>: Indisponibilidade no acesso ao Portal desde 12:01, impactando as consultas de cheque para todos os participantes.
-   <b>Status</b>: N2 Middleware e Redes atuando. Conferência 4502-2772 Sala 709030

# Sistemas Core (DC TIVIT, B3 e BBPROCESSADOR)

### Serviço

-   Neste campo deve ser informado os Produtos e Ambientes.

### Sintoma

-   Neste campo deve ser informado se é uma Indisponibilidade Total / Indisponibilidade Parcial / Lentidão / Enfileiramento. Se não houver impacto, informar “Sem impacto” e se houver impacto informar conforme exemplos abaixo:

Se houver Indisponibilidade / Lentidão / Enfileiramento:

-   Vai gerar atraso? É um Erro ou Falha?
-   Quais (Participantes/Mensagens, Arquivos/Virada de Data/Funcionalidades) são afetadas?
-   Qual Funcionalidade de arquivo afetada?
-   Desde qual horário?
-   Qual interface foi afetada? Connect Direct, CFT, XFB, WebService, Portal ou todas as interfaces?

### Status

-   Neste campo a informação deve referenciar as equipes envolvidas para solução e ações que foram executadas até o presente momento.

# SISTEMAS CORE (DC TIVIT, B3 e BBPROCESSADOR)

### Mensagem de Informação

📣 [INFORMATIVO] EV12345

-   <b>Serviço</b>: PCR PRD
-   <b>Sintoma</b>: Abort na execução varredura PCR0001 (informa status de boletos), meta de negócio 03:59.
-   <b>Status</b>: Sistemas Sustentação e N1 de Banco de Dados atuando.

### Mensagem quando é iniciada uma degradação.

📣 [DEGRADAÇÃO] EV1234

-   <b>Serviço</b>: PCR PRD
-   <b>Sintoma</b>: Abort na execução varredura PCR0001 (informa status de boletos), meta de negócio 03:59.
-   <b>Status</b>: Sistemas Sustentação e N1 de Banco de Dados atuando.

### Mensagem quando degradação é resolvida.

✅ [DEGRADAÇÃO] EV12345

-   <b>Serviço: </b>PCR PRD
-   <b>Sintoma: </b>Falha no inject de arquivos via Connect Direct desde 12:00, impactando a entrega de arquivos para Mercado.
-   <b>Status: </b>Normalizado às 13:22 após o inject manual dos arquivos.

# LINKS DE COMUNICAÇÃO (CORE E CORPORATIVO)

## Links

-   Neste campo deve ser informado Tipo de Link – Operadora – Localizações (Ambiente).

## Sintoma

-   Neste campo deve ser informado se é Indisponibilidade / Intermitência ou Lentidão e desde qual horário.

Se <b>NÃO</b> houver impacto, informar <b>“Sem impacto”</b> e se houver impacto informar conforme exemplos abaixo:

-   Se não houver Impacto: Descrever o motivo de não haver impacto, seja ele por ser atendido por outro circuito ou outros (Ex: Ambiente fora de grade).

-   Se houver impacto: Descrever serviços/ambiente afetados.

## Status

-   Neste campo a informação deve referenciar os acionamentos feitos para os times de Redes, G2N, Operadoras

## Mensagem para Links de comunicação

### Informativo Degradação e Crise

📣 [INFORMATIVO] EV1234

-   <b>Link</b>: RSFN - Primesys - PCR - PRD
-   <b>Sintoma</b>: Circuito apresentando oscilações desde 11:00, impactando envio arquivos para todos os participantes do Mercado.
-   <b>Status</b>: Normalizado às 12:15, após atuação da Operadora, a volta para circuito principal será alinhada com as equipes envolvidas.

📣 [DEGRADAÇÃO] EV1234

-   <b>Link</b>:RSFN - Primesys - PCR - PRD
-   <b>Sintoma</b>: Circuito apresentando oscilações desde 11:00, impactando envio arquivos para todos os participantes do Mercado.
-   <b>Status</b>: N2 Redes realizou a comutação para circuito auxiliar. Todas as trocas de arquivos foram restabelecidas as 11:30. Circuito ainda com degradação. _Checkpoint com operadora agendado para as 12:30_.

📣 [CRISE] EV1234

-   <b>Link</b>:Financial.NET - RTM - C3, SCG e CTC - PRD
-   <b>Sintoma</b>: Indisponibilidade desde 09:00, impactando na troca de arquivos para todos os participantes em todas as interfaces.
-   <b>Status</b>: N2 Redes realizou a comutação para circuito auxiliar, todas as trocas de arquivos foram restabelecidas as 09:30. Circuito principal ainda indisponível.

✅ [INFORMATIVO] EV1234

-   <b>Link</b>: RSFN - Primesys - PCR - PRD
-   <b>Sintoma</b>: Circuito apresentando oscilações desde 11:00, impactando envio arquivos para todos os participantes do Mercado.
-   <b>Status</b>: Normalizado às 12:15, após atuação da Operadora, a volta para circuito principal será alinhada com as equipes envolvidas.

✅ [DEGRADACAO] EV1234

-   <b>Link</b>: RSFN - Primesys - PCR - PRD
-   <b>Sintoma</b>: Circuito apresentando oscilações desde 11:00, impactando envio arquivos para todos os participantes do Mercado.
-   <b>Status</b>: Normalizado às 12:15, após atuação da Operadora, a volta para circuito principal será alinhada com as equipes envolvidas.

✅ [CRISE] EV1234

-   <b>Link</b>: RSFN - Primesys - PCR - PRD
-   <b>Sintoma</b>: Circuito apresentando oscilações desde 11:00, impactando envio arquivos para todos os participantes do Mercado.
-   <b>Status</b>: Normalizado às 12:15, após atuação da Operadora, a volta para circuito principal será alinhada com as equipes envolvidas.

# MUDANÇAS E AVISOS - CORPORATIVO E CORE

[DEPLOY][comutação local][COMUTAÇÃO REMOTA]

-   Tipo da Mudança que será executado.

[MUDANÇA]
