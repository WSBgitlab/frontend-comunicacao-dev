import Api from './../config/api';

import { cardInformation , cardError } from './../config/cards';

import { getToken } from './../config/auth';

//Strings de conecção aos Grupos.
const headers = {
    token : getToken()
}

export async function newEvent(url,relevancia,sistema,ambiente,sintoma,conferencia){
    try{
        let sendMenssagem = `📣 [ <b>ABERTURA</b> - ${relevancia} ]%0A<b>Serviço</b> : ${sistema}%0A<b>Ambiente</b> : ${ambiente}%0A<b>Sintoma</b> : ${sintoma}%0A<b>Conferência</b> : ${conferencia}%0A%0A`;

        const response = await Api.get(`${url}${sendMenssagem}`,{ headers });
        
        if(response.status === 200) return cardInformation('Msg ao telegram enviada com sucesso!');
    }catch(error){
        cardError('Erro interno Telegram, contate suporte!')
    }
}

export async function closedEvent(url,mensseger,sistema , ambiente, sintoma){
    try{
        let sendMenssagem =`✅[<b>Informativo Resolvido</b>]%0A<b>Serviço</b> : ${sistema}%0A<b>Ambiente</b> : ${ambiente}%0A<b>Sintoma</b> : ${sintoma}%0A<b>Solução</b> : ${mensseger}%0A`;

        const response = await Api.get(`${url}${sendMenssagem}`,{ headers })

        if(response.status === 200) return cardInformation("Enviado ao Telegram!")           
    }catch(erro){
        cardError('Erro interno Telegram, contate suporte!')
    }
}

export async function statusForTelegram(url,mensseger,relevancia,sistema,ambiente,sintoma){
    try{
        let sendMenssagem = `📣 [ <b>STATUS</b> - ${relevancia} ]%0A<b>Serviço</b> : ${sistema}%0A<b>Ambiente</b> : ${ambiente}%0A<b>Sintoma</b> : ${sintoma}%0A<b>Informativo</b> : ${mensseger}%0A%0A`;

        const response = await Api.get(`${url}${sendMenssagem}`,{ headers })
    
        if(response.status === 200) return cardInformation("Enviado ao Telegram!")
    }catch(err){
        if (err) throw err;
        cardError("Erro Interno, Erro ao enviar menssagem ao Telegram!");
    }   
}