export function popRto(){
    let principal = document.body;

    let popup = document.createElement('div');
    let divmain = document.createElement('div');
    let buttonAdd = document.createElement('button');
    let buttonClose = document.createElement('button');
    let section = document.createElement('section');

    popup.setAttribute('class','popup');
    buttonAdd.setAttribute('class','btn btn-outline-light');
    buttonClose.setAttribute('class','btn btn-outline-light');
    divmain.setAttribute('class','col col-md-12');
    section.setAttribute('class','section-popup');


    buttonAdd.appendChild(document.createTextNode('Adicionar'));
    buttonClose.appendChild(document.createTextNode('Fechar'));
    
    divmain.appendChild(section);

    section.appendChild(buttonClose);
    section.appendChild(buttonAdd);
    
    popup.appendChild(divmain)
    
    principal.appendChild(popup);

}