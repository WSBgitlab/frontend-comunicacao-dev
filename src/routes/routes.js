import React from "react";

import {
    BrowserRouter as Routers,
    Route,
    Switch,
    Redirect,
} from "react-router-dom";

// import { isAuthenticated } from "./../config/auth";
/**
 * Provider para publicar todas as informações do store para o restante da aplicação
 */

// Components
import Login from "./../pages/Login/index";

import Dashboard from "./../components/Dashboard/index";

import EventAdd from "./../components/Events/New/index";

import EventsOngoing from "./../components/Events/Ongoing/index";

import ChangeOngoing from "./../components/ChangesIT/Ongoing/index";

import InsertNewUser from "./../components/User/Insert/index";

import ClosedEvents from "./../components/Events/Closed/index";

import ClosedChange from "./../components/ChangesIT/Closed/index";

import ViewRedux from "./../components/ViewRedux/index";

import ClosedEventsDetails from "./../components/Events/ClosedDetails/index";

import ClosedChangesDetails from "./../components/ChangesIT/ClosedDetails/index";

import NotFound from "./../components/Screens/notFound/index";

import ResetPassword from "./../components/Screens/ResetPassword/index";

// import DataPagination from './../components/DatasPaginations/index';

import DatasPaginations from "./../components/Paginate/DatasPaginations/index";

// eslint-disable-next-line react/prop-types
const PrivateRoute = ({ component: Component, ...rest }) => (
    <Route
        {...rest}
        render={(props) =>
            localStorage.getItem("keT") !== null ? (
                <Component {...props} />
            ) : (
                <Redirect to={{ pathname: "/", state: { from: props } }} />
            )
        }
    />
);

export default function Router() {
    return (
        <Routers>
            <Switch>
                <Route path="/" exact component={Login} />
                <PrivateRoute path="/notFound" component={NotFound} />
                <PrivateRoute path="/pagination" component={DatasPaginations} />
                <Route path="/dashboard" component={Dashboard} />
                <PrivateRoute path="/event/add" component={EventAdd} />
                <PrivateRoute path="/view/redux" component={ViewRedux} />
                <Route path="/users/new" component={InsertNewUser} />
                <PrivateRoute path="/event/ongoing/:id_event/:id_user" component={EventsOngoing} />
                <PrivateRoute path="/event/change/ongoing/:id_change/:id_user" component={ChangeOngoing} />
                <Route path="/reset/password/" component={ResetPassword} />
                <PrivateRoute path="/delete/events/:id_event" component={ClosedEvents} />
                <PrivateRoute path="/delete/changes/" component={ClosedChange} />
                <PrivateRoute path="/delete/events/details/:id_event" component={ClosedEventsDetails} />
                <PrivateRoute path="/delete/changes/details/:id_change" component={ClosedChangesDetails} />
            </Switch>
        </Routers>
    );
}
