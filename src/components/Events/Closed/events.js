import React, { useEffect, useState } from "react";

import Api from "../../../config/api";

import { Pagination } from '@material-ui/lab'

import { useHistory } from 'react-router-dom';

import Events from './content';

const EventsClosed = () => {
    const [eventsClosed, setEventsClosed] = useState([]);
    const [loading, setLoad] = useState(false);

    const history = useHistory();
    
    const [currentPage, setCurrentPage] = useState(1);
    const [eventsPerPages] = useState(5);
    
    // Obtendo os Events atuais
    const indexOfLastEvent = currentPage * eventsPerPages;
    const indexOfFirstEvent = indexOfLastEvent - eventsPerPages;
    const currentEvent = eventsClosed.slice(indexOfFirstEvent, indexOfLastEvent);
    
    // Events Closed
    useEffect(() => {
        async function loadEventsClosed() {
            try{
                setLoad(true)
                let eventsClosed = await Api.get(
                    "/api/v1/events/list/close",
                    {
                        headers : {
                            token : localStorage.getItem("keT")
                        }
                    }
                );
                setLoad(false)
                setEventsClosed(eventsClosed.data);
            }catch(err){
                history.push("/")
            }
        }

        loadEventsClosed();
    },[]);

    return (
        <>
            
            <Events 
                events={currentEvent} 
                loading={loading}
            />

            {/* <span>{Math.ceil(eventsClosed.length / eventsPerPages)}</span> */}
            
            <div className="col col-md-12 pagination-grid">
                <Pagination count={Math.ceil(eventsClosed.length / eventsPerPages)} onChange={(event , page) => {
                    event.preventDefault();
                    setCurrentPage(page)
                }}/>
            </div>
        </>
    );
};

export default EventsClosed;
