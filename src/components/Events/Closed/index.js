import React from "react";

import ClosedTitle from './../../../assets/icons/close.png';

import EventsClosed from './events';

export default function EventsClose() {
    return (
        <>
            <div className="header-tables">
                <h3>
                    Eventos Fechados
                    <img src={ClosedTitle} alt=""></img>
                </h3>
                <hr />
            </div>
            <div className="tables">
                <EventsClosed></EventsClosed>  
            </div>
        </>
    );
}
