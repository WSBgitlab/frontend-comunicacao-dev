/* eslint-disable react/prop-types */
import React from "react";

import { formatDate } from "../../../utils/dateFormats";

import { Button, Spinner } from "react-bootstrap";

import { makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import Container from "@material-ui/core/Container";

const useStyles = makeStyles({
    table: {
        minWidth: 650,
    },
});

const EventsOpen = ({ events, loading }) => {
    const classes = useStyles();

    if (loading)
        return (
            <Button variant="primary" disabled>
                <Spinner
                    as="span"
                    animation="grow"
                    size="sm"
                    role="status"
                    aria-hidden="true"
                />
                Carregando ...
            </Button>
        );

    return (
        <>
            {events.length === 0 ? (
                <Container style={{ textAlign: "center" }}>
                    <h4>Não há Eventos fechados!</h4>
                </Container>
            ) : (
                <TableContainer style={{ height: "400px" }} component={Paper}>
                    <Table className={classes.table} aria-label="simple table">
                        <TableHead>
                            <TableRow
                                style={{ background: "rgb(149, 152, 169)" }}
                            >
                                <TableCell className="tr-header">
                                    Status
                                </TableCell>
                                <TableCell className="tr-header" align="left">
                                    Sintoma
                                </TableCell>
                                <TableCell className="tr-header" align="left">
                                    Evento
                                </TableCell>
                                <TableCell className="tr-header" align="left">
                                    Última Atualização
                                </TableCell>
                                <TableCell className="tr-header" align="left">
                                    Relevância
                                </TableCell>
                                <TableCell className="tr-header" align="left">
                                    Sistema
                                </TableCell>
                                <TableCell className="tr-header" align="left">
                                    Ambiente
                                </TableCell>
                                <TableCell className="tr-header" align="left">
                                    Conferência
                                </TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {events.map((data) => (
                                <TableRow key={data.status}>
                                    <TableCell component="th" scope="row">
                                        {data.status}
                                    </TableCell>
                                    <TableCell align="left">
                                        {data.sintoma}
                                    </TableCell>
                                    <TableCell align="left">
                                        {data.evento}
                                    </TableCell>
                                    <TableCell align="left">
                                        {formatDate(data.updatedAt)}
                                    </TableCell>
                                    <TableCell align="left">
                                        {data.relevancia}
                                    </TableCell>
                                    <TableCell align="left">
                                        {data.sistema}
                                    </TableCell>
                                    <TableCell align="left">
                                        {data.ambiente}
                                    </TableCell>
                                    <TableCell align="left">
                                        {data.conferencia}
                                    </TableCell>
                                </TableRow>
                            ))}
                        </TableBody>
                    </Table>
                </TableContainer>
            )}
        </>
    );
};

export default EventsOpen;
