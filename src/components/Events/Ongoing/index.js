/* eslint-disable react/prop-types */
import React, { useState , useEffect} from "react";

import { useDispatch } from "react-redux";

import { cardInformation, cardError } from "./../../../config/cards";

import Api from "./../../../config/api";

import { getToken } from "./../../../config/auth";

import { useHistory } from "react-router-dom";

import Menu from "../../Screens/Menu/index";

import { Button , Spinner } from "react-bootstrap";

import MainAppStyled from "../../Screens/MainApp/styled";

import StyledDashboard from "../../Dashboard/styled";

import StyledOngoingEvents from "./styled";

import io from "socket.io-client";

import api from "./../../../config/api";

import iconSendMessage from "./../../../assets/icons/send_comment.png";

import { formatDate } from "./../../../utils/dateFormats";

export default function Index(props){
    let urlSocketIO = "http://localhost:8080";

    if(process.env.NODE_ENV === "production"){
        urlSocketIO = "https://portaldecomunicacao.cip.lan.com";
    }

    //Comentários que o usuário inserir e será enviados na timeline
    const [sendComment, setSendComment] = useState("");

    //Comentários retornados da api
    const [comments, setComments] = useState([]);

    const [msgTeams, setMsgTeams] = useState("");

    const [handleComponent, setHandleComponent] = useState(4);

    const [loading,setLoading] = useState(false);

    const [lenCharComments, setLenCharComments] = useState(0);
    //Variáveis dos grupos, se caso estiver como true enviar a mensagem.
    const dispatch = useDispatch();

    //Colocar a url do DNS ou IP do host que está nossa api
    const socket = io.connect(urlSocketIO, {
        transports : ['websocket','polling'],
        upgrade : true,
        rejectUnauthorized: true,
        autoConnect: true,
        timeout: 2000,
        reconnectionDelay:1000
    });

    const history = useHistory();

    const { id_event } = props.match.params;

    document.title = `Detalhes Evento ${props.location.state.evento}`

    try{
        socket.on("Comments", (newComments) => {
          setHandleComponent(handleComponent + 1);
       
          setComments([...comments, newComments]);

            setTimeout(() => {
                console.log("depois de 6 segundos");
                socket.disconnect();
            },6000)
        });


        
    }catch(err){
        socket.on('disconnect', (reason) => {
          if (reason === 'io server disconnect') {
            // the disconnection was initiated by the server, you need to reconnect manually
            socket.connect();
          }
        });
    }


    useEffect(() =>{
        document.querySelector("section.comments").scrollTop = 500000; 
    })

    useEffect(() => {
        async function changeLength() {
            setLenCharComments(sendComment.length);
        }

        changeLength();
    },[sendComment])


    useEffect(() => {
        async function getComments() {
            const response = await api.get(
                // eslint-disable-next-line react/prop-types
                `/api/v1/events/comments/list/${id_event}`,
                {
                    headers: {
                        token: localStorage.getItem("keT"),
                    },
                }
            );
            
            setComments(response.data);
        }
        

        getComments();
    }, [handleComponent]);

    async function handleSubmit(e) {
        e.preventDefault();

        const token = localStorage.getItem("keT");
        const id_users = parseInt(localStorage.getItem("upid"));
        // eslint-disable-next-line react/prop-types
        const id_events = parseInt(props.match.params.id_event);

        if (sendComment === "") {
            return cardError("Insira o comentário!");
        } else {
            let obj = {
                token,
                id_events,
                id_users,
                comments: sendComment,
            };

            await api.post(
                "/api/v1/events/insert/comments",
                obj,
                {
                    headers: {
                        token,
                    },
                }
            ).then(res => {
                if (res.status === 200) {
                    cardInformation("Comentário Inserido!");
                    document.querySelector("section.comments").scrollTop = 50000;
                    setSendComment("");
                    // Apagar string
                    document.getElementById("textSendComment").value = "";
                    // Zerar o text area, somente a variável mas não a parte visual.
                    setHandleComponent(handleComponent + 2);
                }
            }).catch(err => {
                console.log(err, "erro")
                history.push("/");
            })
        }
    }

    async function handleTeamsSubmit(event) {
        event.preventDefault();
        try {

            setLoading(true)
            await Api.post("api/v1/users/validate/token", {
                token: getToken(),
            },{
                headers :{
                    token : localStorage.getItem("keT")
                }
            }).then(async (res) => {
                if (res.data.status === undefined || res.data.status === false) {
                    history.push("/");
                } else {
                    const teamsMessage = await api.post(
                        "/api/v1/bot/send/message/event/teams",
                        {   
                            id_message : 3,
                            text: msgTeams,
                            relevancia : props.location.state.relevancia,
                            evento : props.location.state.evento,
                            servico : props.location.state.servico,
                            ambiente : props.location.state.ambiente,
                            sintoma : props.location.state.sintoma,
                            status : props.location.state.status
                        },
                        {
                            headers: {
                                token: localStorage.getItem("keT"),
                            },
                        }
                    );

                    if ((teamsMessage.data.statusCodeUserBot === true)) {
                        cardInformation("Mensagem enviada com sucesso!");
                        setMsgTeams("");
                        setLoading(false);
                        return true;
                    } else {
                        cardError("Mensagem não foi enviada com sucesso!");
                        setLoading(false);
                        setMsgTeams("");
                        return false;
                    }
                }
            });
        } catch (err) {
            setLoading(false);
            cardError(err, "Erro ao enviar mensagem ");
        }
    }

    return (
        <>
            <Menu />
            <StyledDashboard>
                <MainAppStyled className="col col-md-10 maingrid-dash">
                    <StyledOngoingEvents>
                        <div className="header">
                            <header>
                                <span>
                                    <h3>Acompanhamento de Eventos</h3>
                                </span>
                                <hr></hr>
                            </header>
                            <div 
                                className="container"
                                style={{ textAlign : "right", boxSizing : "content-box"}}
                            >
                                <button className="btn btn-outline-danger" onClick={() =>{
                                    history.push(`/delete/events/${props.match.params.id_event}`,{
                                        id_event : props.location.state.id_event,
                                        ambiente: props.location.state.ambiente,
                                        evento: props.location.state.evento,
                                        conferencia: props.location.state.conferencia,
                                        relevancia: props.location.state.relevancia,
                                        servico: props.location.state.servico,
                                        sintoma: props.location.state.sintoma,
                                        status: props.location.state.status,
                                        createdAt : props.location.state.createdAt,
                                        updatedAt : props.location.state.updatedAt
                                    })
                                }}>Fechar Evento</button>
                            </div>
                        </div>

                        <main>
                            <div className="col col-md-12 header-boxes">
                                <div className="header-main-boxes">
                                    <h3>Sistema : {props.location.state.servico}</h3>
                                    <hr />
                                </div>
                                <div className="col col-md-12 main-grid-box">
                                    <div className="col col-md-5 box box-crise">
                                        <header className="box-header">
                                            <b>Informações Crise</b>
                                        </header>
                                        <nav>
                                            <ul>
                                                <li>
                                                    <b>Ambiente</b> :
                                                    <i className="ml-5">
                                                        {props.location.state.ambiente}
                                                    </i>
                                                </li>
                                                <li>
                                                    <b>Data de criação</b>:
                                                    <i className="ml-5">
                                                        {formatDate(props.location.state.createdAt)}
                                                    </i>
                                                </li>
                                                <li>
                                                    <b>Data de modificações</b>:
                                                    <i className="ml-5">
                                                        {formatDate(props.location.state.updatedAt)}
                                                    </i>
                                                </li>
                                            </ul>
                                        </nav>
                                    </div>
                                    <div className="col col-md-5 box box-event">
                                        <header className="box-header">
                                            <b>Informações Evento</b>
                                        </header>
                                        <nav>
                                            <ul>
                                                <li>
                                                    <b>Evento </b>:
                                                    <i className="ml-5">
                                                        {props.location.state.evento}
                                                    </i>
                                                </li>
                                                <li>
                                                    <b>Sintoma </b>:
                                                    <i className="ml-5">
                                                        {props.location.state.sintoma}
                                                    </i>
                                                </li>
                                                <li>
                                                    <b>Relevância </b>:
                                                    <i className="ml-5">
                                                        {props.location.state.relevancia}
                                                    </i>
                                                </li>
                                                <li>
                                                    <b>Sistema </b>:
                                                    <i className="ml-5">
                                                        {props.location.state.servico}
                                                    </i>
                                                </li>
                                            </ul>
                                        </nav>
                                    </div>
                                </div>
                            </div>

                            <div className="header-main-box">
                                <div className="header-main-boxes">
                                    <h3>Detalhes :</h3>
                                    <hr></hr>
                                </div>

                                <div className="col col-md-12 main-grid-box timeline">
                                    <div
                                        className="col col-md-8 box box-event box-comments"
                                        style={{
                                            padding: "0px",
                                            height: "358px",
                                            background: "#b6b6b533",
                                        }}
                                    >
                                        <div className="teste"></div>

                                        <section className="comments">
                                            {comments.map((item) => (
                                                <>
                                                    <div key={item.id} className="comments text-break">
                                                        <div className="header-comments">
                                                            <b>{item.nome} - {formatDate(item.updated_at)}</b>
                                                        </div>
                                                        <div className="main-comments">
                                                            <p>
                                                                {
                                                                    item.comentario
                                                                }
                                                            </p>
                                                        </div>
                                                    </div>
                                                </>
                                            ))}
                                        </section>
                                    </div>

                                    <div className="col col-md-8 send-comments">
                                        <form onSubmit={handleSubmit}>
                                            
                                            <input
                                                className="form-control"
                                                type="text"
                                                id="textSendComment"
                                                onChange={(event) =>
                                                    setSendComment(
                                                        event.target.value
                                                    )
                                                }
                                            />
                                            <div
                                                className="input"
                                                onClick={handleSubmit}
                                            >
                                                <b>{lenCharComments} / 10</b>
                                                <img
                                                    src={iconSendMessage}
                                                    alt=""
                                                ></img>
                                                <input type="hidden" />
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>

                            <div className="col col-md-12">
                                <div className="header-main-boxes">
                                    <h3>Enviar mensagem para Teams</h3>
                                    <hr></hr>
                                </div>

                                <form
                                    style={{
                                        width: "100%",
                                        display: "flex",
                                        flexDirection: "column",
                                        alignItems: "center",
                                        justifyContent: "center",
                                    }}
                                    onSubmit={handleTeamsSubmit}
                                >
                                    <textarea
                                        className="form-control"
                                        style={{
                                            width: "60%",
                                            height: "100px",
                                            display: "flex",
                                            margin: "1em 2em",
                                        }}
                                        onChange={(event) =>
                                            setMsgTeams(event.target.value)
                                        }
                                    ></textarea>
                                {
                                    loading === false  ? 
                                        <button
                                            type="submit"
                                            className="btn btn-outline-info"
                                            style={{
                                                width: "150px",
                                                margin: "0 auto",
                                            }}
                                        >Enviar{" "}</button> : 
                                        <Button variant="primary" disabled>
                                            <Spinner
                                                as="span"
                                                animation="grow"
                                                size="sm"
                                                role="status"
                                                aria-hidden="true"
                                            />Loading...
                                        </Button>
                                    }
                                    <br></br>
                                    <br></br>
                                    <br></br>
                                </form>
                            </div>
                        </main>
                    </StyledOngoingEvents>
                </MainAppStyled>
            </StyledDashboard>        
        </>
    )
}