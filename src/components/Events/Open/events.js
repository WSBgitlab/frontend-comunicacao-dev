import React, { useEffect, useState } from "react";

import Api from "../../../config/api";

import { Pagination } from '@material-ui/lab'

import { useHistory } from 'react-router-dom';


import Events from './content';

// eslint-disable-next-line react/prop-types
const EventsOpen = () => {
    const [eventsOpen, setEventsOpen] = useState([]);
    const [loading, setLoad] = useState(false);

    const history = useHistory();
    
    const [currentPage, setCurrentPage] = useState(1);
    const [eventsPerPages] = useState(5);
    
    // Obtendo os Events atuais
    const indexOfLastEvent = currentPage * eventsPerPages;
    const indexOfFirstEvent = indexOfLastEvent - eventsPerPages;
    const currentEvent = eventsOpen.slice(indexOfFirstEvent, indexOfLastEvent);
    
    // Events Open
    useEffect(() => {
        async function loadEventsOpen() {
            setLoad(true)
            try{
                let eventsOpen = await Api.get(
                    "/api/v1/events/list/open",
                    {
                        token : localStorage.getItem("keT")
                    }
                );
                setLoad(false)
                setEventsOpen(eventsOpen.data);
            }catch(err){
                history.push("/")
            }
        }

        loadEventsOpen();
    },[]);

    return (
        <>
            <Events 
                events={currentEvent} 
                loading={loading}
            />

            {/* <span>{Math.ceil(eventsOpen.length / eventsPerPages)}</span> */}
            
            <div className="col col-md-12 pagination-grid">
                <Pagination count={Math.ceil(eventsOpen.length / eventsPerPages)} onChange={(event , page) => {
                    event.preventDefault();
                    setCurrentPage(page)
                }}/>
            </div>
        </>
    );
};

export default EventsOpen;
