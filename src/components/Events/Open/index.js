import React from "react";

import SucessTitle from "./../../../assets/icons/open.png";

import EventOpen from './events';

export default function EventsOpen(props) {
    return (
        <>
            <div className="header-tables">
                <h3>
                    Eventos Abertos
                    <img src={SucessTitle} alt=""></img>
                </h3>
                <hr />
            </div>
            <div className="tables">
                <EventOpen></EventOpen>  
            </div>
        </>
    );
}
