import React, { useState, useEffect } from "react";

import { Link } from "react-router-dom";

import Styled from "./styled";

import Menu from "./../../Screens/Menu/index";

import ChangeEvents from "./formChange";

import { cardError, cardInformation, cardSucess } from "../../../config/cards";

import MenuAppStyled from "./../../Screens/MainApp/styled";

import StyledEvents from "./styled";

import api from "../../../config/api";

import iconQuestion from "./../../../assets/icons/questions.png";

import { useHistory } from 'react-router-dom';

export default function AddEvents(props) {
    const [sintoma, setSintoma] = useState("");
    const [relevancia, setRelevancia] = useState("");
    const [system, setSystem] = useState("");
    const [ambiente, setAmbiente] = useState("");
    const [statusCrise, setStatusCrise] = useState("");
    const [evento, setEvento] = useState("");
    const [conferencia, setConferencia] = useState("");

    const [mudanca, setMudanca] = useState(false);

    const [systemOtherControl, setsystemOtherControl] = useState(false);
    const [othersSystems, setOthersSystem] = useState("");

    const history = useHistory();

    document.title = "Adicionar Evento";

    useEffect(() => {
        function addOtherSistem() {
            if (system === "Other") {
                document
                    .getElementById("otherSistem")
                    .classList.add("show-others-sistem");
                setsystemOtherControl(true);
            } else if (systemOtherControl === false) {
                document
                    .getElementById("otherSistem")
                    .classList.remove("show-others-sistem");
            } else {
                document
                    .getElementById("otherSistem")
                    .classList.remove("show-others-sistem");
            }
        }

        addOtherSistem();
    }, [systemOtherControl]);

    async function handleSubmit(event) {
        event.preventDefault();

        if ((sintoma || relevancia || system || ambiente || system || statusCrise) === "") {
            cardError("Preencha os campos corretamente");
            return false;
        }

        // Adicionar evento quando for outro sistema escolhido pelo usuário
        if (othersSystems !== "") {
            if ((sintoma || relevancia || othersSystems || ambiente || statusCrise) === "") {
                cardError("Preencha os campos corretamente");
                return false;
            }
            api.post(
                "/api/v1/events/create",
                {
                    status: statusCrise,
                    sintoma,
                    relevancia,
                    sistema: othersSystems,
                    ambiente,
                    evento,
                    conferencia,
                    token: localStorage.getItem("keT"),
                },
                {
                    headers: {
                        token: localStorage.getItem("keT"),
                    },
                }
            ).then(res => {
                if(res.status === 200){
                    cardInformation("Evento cadastrado com sucesso!");
                    // eslint-disable-next-line react/prop-types
                    props.history.push("/dashboard");
                }

                if(res.status === 400){
                    cardError("Preencha os campos corretamente");
                }
            }).catch(err => {
                cardError("Preencha os campos corretamente!");
                console.log(err);
            })

        } else {
            api.post(
                "/api/v1/events/create",
                {
                    status: statusCrise,
                    sintoma,
                    relevancia,
                    sistema: system,
                    ambiente,
                    evento,
                    conferencia,
                    token: localStorage.getItem("keT"),
                }
            ).then(res => {
                if(res.status === 200){
                    cardSucess("Evento Cadastrado!")
                    history.push("/dashboard")
                }
            }).catch(err => {
                cardError("Preencha os campos corretamente!")
            })
        }
    }

    // Criar funcionalidade  de envio de mensagens ao teams.

    return (
        <>
            <Menu />
            <Styled>
                <MenuAppStyled
                    style={{ backgroundColor: "transparent" }}
                    className="col col-md-10 maingrid-dash"
                >
                    <StyledEvents>
                        <header>
                            <span>
                                <h4>Portal de Comunicação - Adicionar Evento </h4>
                            </span>
                            <hr></hr>
                        </header>

                        <main className="addmenu">
                            {
                                // Serviços
                            }
                            <div className="div-label-form-events servicos">
                                <div className="div-label-form-events" id="">
                                    <label
                                        className="label-form-color"
                                        htmlFor="System"
                                    >
                                        <strong>Serviços</strong>
                                    </label>
                                    <select
                                        className="form-control"
                                        onChange={(event) => {
                                            if (event.target.value === "Other") {
                                                document
                                                    .getElementById("otherSistem")
                                                    .classList.remove(
                                                        "display-none"
                                                    );

                                                document
                                                    .getElementById("otherSistem")
                                                    .classList.add("display-flex");
                                            }

                                            setSystem(event.target.value);
                                        }}
                                    >
                                        <option defaultValue="">Selecione</option>
                                        <option value="C3">C3</option>
                                        <option value="CPO">CPO</option>
                                        <option value="CQL">CQL</option>
                                        <option value="CTC">CTC</option>
                                        <option value="VDI">
                                            Desktops Virtuais
                                        </option>
                                        <option value="Exchange">
                                            E-mail Corporativo
                                        </option>
                                        <option value="LINK">LINK</option>
                                        <option value="MCB">MCB</option>
                                        <option value="Portal do Participante">
                                            Portal do Participante
                                        </option>
                                        <option value="PCR">PCR</option>
                                        <option value="File Server">
                                            Servidor de Arquivos
                                        </option>
                                        <option value="SILOC">SILOC</option>
                                        <option value="SITRAF">SITRAF</option>
                                        <option value="SCC">SCC</option>
                                        <option value="SERAP">SERAP</option>
                                        <option value="SLC">SLC</option>
                                        <option value="STD">STD</option>
                                        <option value="PABX">Telefonia</option>
                                        <option value="Other">Outro</option>
                                    </select>

                                    <div
                                        className="col-md-12 display-none"
                                        id="otherSistem"
                                    >
                                        <label className="label-form-color">
                                            <strong>Outro Serviço :</strong>
                                        </label>
                                        <input
                                            type="text"
                                            className="form-control input-form-event-conferencia"
                                            id="input-system-other"
                                            onChange={(event) => {
                                                setOthersSystem(event.target.value);
                                            }}
                                        />
                                    </div>

                                    <span>
                                        <img
                                            src={iconQuestion}
                                            alt="question display-none"
                                            onClick={(event) => {
                                                if (
                                                    document
                                                        .getElementById(
                                                            "information-servico"
                                                        )
                                                        .classList.contains(
                                                            "display-none"
                                                        )
                                                ) {
                                                    document
                                                        .getElementById(
                                                            "information-servico"
                                                        )
                                                        .classList.remove(
                                                            "display-none"
                                                        );
                                                } else {
                                                    document
                                                        .getElementById(
                                                            "information-servico"
                                                        )
                                                        .classList.add(
                                                            "display-none"
                                                        );
                                                }
                                            }}
                                        ></img>
                                    </span>

                                    <div
                                        className="display-none question"
                                        id="information-servico"
                                    >
                                        <strong>Serviço</strong>
                                        <p>
                                            Neste campo deve ser informado os Produtos e Ambientes.
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <form
                                id="form-event"
                                className="form-add-event"
                                onSubmit={handleSubmit}
                            >
                                {
                                    // Relevância
                                }
                                <span id="span-form">
                                    <div
                                        className="div-label-form-events"
                                        id=""
                                    >
                                        <label
                                            className="label-form-color"
                                            htmlFor="Relevância"
                                        >
                                            <strong>Tipo da Crise</strong>
                                        </label>
                                        <select
                                            className="form-control"
                                            onChange={(event) => { 
                                                    setRelevancia(
                                                        event.target.value
                                                    )

                                                    if(event.target.value === "MUDANCA"){
                                                        setMudanca(true);
                                                    }else{
                                                        setMudanca(false);
                                                    }
                                                }
                                            }
                                        >
                                            <option defaultValue="">
                                                Selecione
                                            </option>
                                            <option value="Catastrofe">
                                                Catástrofe
                                            </option>
                                            <option value="Crise">
                                                Crise TI
                                            </option>
                                            <option value="Informativo">
                                                Informativo
                                            </option>
                                            <option value="Degradacao">
                                                Degradação
                                            </option>
                                            <option value="MUDANCA">
                                                Mudança
                                            </option>
                                        </select>
                                        <span>
                                            <img
                                                src={iconQuestion}
                                                alt="question"
                                                onClick={(event) => {
                                                    if (
                                                        document
                                                            .getElementById(
                                                                "information-relevancia"
                                                            )
                                                            .classList.contains(
                                                                "display-none"
                                                            )
                                                    ) {
                                                        document
                                                            .getElementById(
                                                                "information-relevancia"
                                                            )
                                                            .classList.remove(
                                                                "display-none"
                                                            );
                                                    } else {
                                                        document
                                                            .getElementById(
                                                                "information-relevancia"
                                                            )
                                                            .classList.add(
                                                                "display-none"
                                                            );
                                                    }
                                                }}
                                            ></img>
                                        </span>
                                        <div
                                            className="display-none question"
                                            id="information-relevancia"
                                        >
                                            <strong>Tipo da crise</strong>
                                            <p>
                                                Escolha uma opção que seja
                                                referente a sua crise a ser
                                                iniciada.
                                            </p>

                                            <ul>
                                                <li>Catástrofe</li>
                                                <li>Crise TI</li>
                                                <li>Informativo</li>
                                                <li>Degradação</li>
                                            </ul>
                                        </div>
                                    </div>
                                    {
                                        mudanca === true ? (
                                            <ChangeEvents />
                                        ) : (
                                            <>
                                            <div className="div-label-form-events">
                                                <label className="label-form-color lbl-sintoma">
                                                    <strong>Sintoma</strong>
                                                </label>
                                                <input
                                                    type="text"
                                                    className="form-control input-form-event-sintoma"
                                                    onChange={(event) =>
                                                        setSintoma(event.target.value)
                                                    }
                                                    placeholder="Por favor descreva o sintoma dessa crise."
                                                />
                                                <span>
                                                    <img
                                                        src={iconQuestion}
                                                        alt="question"
                                                        onClick={(event) => {
                                                            if (
                                                                document
                                                                    .getElementById(
                                                                        "information-sintoma"
                                                                    )
                                                                    .classList.contains(
                                                                        "display-none"
                                                                    )
                                                            ) {
                                                                document
                                                                    .getElementById(
                                                                        "information-sintoma"
                                                                    )
                                                                    .classList.remove(
                                                                        "display-none"
                                                                    );
                                                            } else {
                                                                document
                                                                    .getElementById(
                                                                        "information-sintoma"
                                                                    )
                                                                    .classList.add(
                                                                        "display-none"
                                                                    );
                                                            }
                                                        }}
                                                    ></img>
                                                </span>
                                                <div
                                                    className="question display-none"
                                                    id="information-sintoma"
                                                >
                                                    <strong>Sintoma</strong>
                                                    <br></br>
                                                    <p>
                                                        Neste campo deve ser informado
                                                        se é uma{" "}
                                                        <b style={{ color: "#a5c8ff" }}>
                                                            <i>
                                                                Indisponibilidade Total
                                                            </i>
                                                        </b>{" "}
                                                        /{" "}
                                                        <b style={{ color: "#a5c8ff" }}>
                                                            Indisponibilidade Parcial
                                                        </b>{" "}
                                                        /{" "}
                                                        <b style={{ color: "#a5c8ff" }}>
                                                            Lentidão
                                                        </b>{" "}
                                                        /{" "}
                                                        <b style={{ color: "#a5c8ff" }}>
                                                            Enfileiramento
                                                        </b>{" "}
                                                        <p>
                                                            Se não houver impacto,
                                                            informar{" "}
                                                            <b
                                                                style={{
                                                                    color: "#a5c8ff",
                                                                }}
                                                            >
                                                                “Sem impacto”
                                                            </b>{" "}
                                                            e se houver impacto informar
                                                            conforme exemplos abaixo:
                                                        </p>
                                                        <br></br>
                                                        <br></br>
                                                        <p>
                                                            Se houver Indisponibilidade
                                                            / Lentidão / Enfileiramento:
                                                        </p>
                                                        <ul>
                                                            <li>
                                                                Vai gerar atraso? É um
                                                                Erro ou Falha?
                                                            </li>
                                                            <li>
                                                                Quais
                                                                (Participantes/Mensagens,
                                                                Arquivos/Virada de
                                                                Data/Funcionalidades)
                                                                são afetadas?
                                                            </li>
                                                            <li>
                                                                Qual Funcionalidade de
                                                                arquivo afetada?
                                                            </li>
                                                            <li>Desde qual horário?</li>
                                                            <li>
                                                                Qual interface foi
                                                                afetada? Connect Direct,
                                                                CFT, XFB, WebService,
                                                                Portal ou todas as
                                                                interfaces?
                                                            </li>
                                                        </ul>
                                                    </p>
                                                    <br></br>
                                                    <strong>Exemplo :</strong>
                                                    <p>
                                                        Falha no inject de arquivos via
                                                        Connect Direct desde 12:00,
                                                        impactando a entrega de arquivos
                                                        para Mercado.
                                                    </p>
                                                </div>
                                            </div>
                                            <div className="div-label-form-events">
                                                <label
                                                    className="label-form-color"
                                                    htmlFor="Ambiente"
                                                >
                                                    <strong>Ambiente</strong>
                                                </label>
                                                <select
                                                    className="form-control"
                                                    onChange={(event) =>
                                                        setAmbiente(event.target.value)
                                                    }
                                                >
                                                    <option defaultValue="">
                                                        Selecione
                                                    </option>
                                                    <option value="DR">DR</option>
                                                    <option value="HEXT">HEXT</option>
                                                    <option value="PRD">PRD</option>
                                                    <option value="PRD e HEXT">
                                                        PRD e HEXT
                                                    </option>
                                                </select>
                                                <span>
                                                    <img
                                                        src={iconQuestion}
                                                        alt="question"
                                                        onClick={(event) => {
                                                            if (
                                                                document
                                                                    .getElementById(
                                                                        "information-ambiente"
                                                                    )
                                                                    .classList.contains(
                                                                        "display-none"
                                                                    )
                                                            ) {
                                                                document
                                                                    .getElementById(
                                                                        "information-ambiente"
                                                                    )
                                                                    .classList.remove(
                                                                        "display-none"
                                                                    );
                                                            } else {
                                                                document
                                                                    .getElementById(
                                                                        "information-ambiente"
                                                                    )
                                                                    .classList.add(
                                                                        "display-none"
                                                                    );
                                                            }
                                                        }}
                                                    ></img>
                                                </span>
                                                <div className="display-none question">
                                                    <strong>Ambiente</strong>
                                                    <p>
                                                        Qual ambiente o serviço está{" "}
                                                        <b style={{ color: "#a5c8ff" }}>
                                                            hospedado
                                                        </b>{" "}
                                                        /{" "}
                                                        <b style={{ color: "#a5c8ff" }}>
                                                            executando
                                                        </b>{" "}
                                                        .
                                                    </p>
                                                    <br></br>
                                                    <strong>Exemplo :</strong>
                                                    <p>PRD ou HEXT</p>
                                                </div>
        
                                                <div
                                                    className="display-none question"
                                                    id="information-ambiente"
                                                >
                                                    <strong>Ambiente</strong>
                                                    <p>
                                                        Escolha uma opção que seja
                                                        referente ao ambiente em que o
                                                        sistema está sendo{" "}
                                                        <b style={{ color: "#a5c8ff" }}>
                                                            hospedado
                                                        </b>{" "}
                                                        /{" "}
                                                        <b style={{ color: "#a5c8ff" }}>
                                                            executando
                                                        </b>{" "}
                                                        .
                                                    </p>
                                                </div>
                                            </div>
                                            
                                            <div className="div-label-form-events">
                                                <label
                                                    className="label-form-color"
                                                    htmlFor="Statys"
                                                >
                                                    <strong>Status</strong>
                                                </label>
                                                <input
                                                    type="text"
                                                    className="form-control input-form-event-conferencia"
                                                    onChange={(event) =>
                                                        setStatusCrise(
                                                            event.target.value
                                                        )
                                                    }
                                                    placeholder="Status da crise"
                                                />
                                                <span>
                                                    <img
                                                        src={iconQuestion}
                                                        alt="question"
                                                        onClick={(event) => {
                                                            if (
                                                                document
                                                                    .getElementById(
                                                                        "information-status"
                                                                    )
                                                                    .classList.contains(
                                                                        "display-none"
                                                                    )
                                                            ) {
                                                                document
                                                                    .getElementById(
                                                                        "information-status"
                                                                    )
                                                                    .classList.remove(
                                                                        "display-none"
                                                                    );
                                                            } else {
                                                                document
                                                                    .getElementById(
                                                                        "information-status"
                                                                    )
                                                                    .classList.add(
                                                                        "display-none"
                                                                    );
                                                            }
                                                        }}
                                                    ></img>
                                                </span>
                                                <div
                                                    className="display-none question"
                                                    id="information-status"
                                                >
                                                    <strong>Status</strong>
                                                    <p>
                                                        Neste campo a informação deve
                                                        referenciar as equipes
                                                        envolvidas para solução e ações
                                                        que foram executadas até o
                                                        presente momento.
                                                    </p>
                                                </div>
                                            </div>
                                            
                                            <div className="div-label-form-events">
                                                <label
                                                    className="label-form-color"
                                                    htmlFor="Ambiente"
                                                >
                                                    <strong>Evento</strong>
                                                </label>
                                                <input
                                                    type="text"
                                                    className="form-control input-form-event-conferencia"
                                                    onChange={(event) =>
                                                        setEvento(event.target.value)
                                                    }
                                                    placeholder="Inserir Evento Ex: EV10032C3"
                                                />
                                                <span>
                                                    <img
                                                        src={iconQuestion}
                                                        alt="question"
                                                        onClick={(event) => {
                                                            if (
                                                                document
                                                                    .getElementById(
                                                                        "information-evento"
                                                                    )
                                                                    .classList.contains(
                                                                        "display-none"
                                                                    )
                                                            ) {
                                                                document
                                                                    .getElementById(
                                                                        "information-evento"
                                                                    )
                                                                    .classList.remove(
                                                                        "display-none"
                                                                    );
                                                            } else {
                                                                document
                                                                    .getElementById(
                                                                        "information-evento"
                                                                    )
                                                                    .classList.add(
                                                                        "display-none"
                                                                    );
                                                            }
                                                        }}
                                                    ></img>
                                                </span>
                                                <div
                                                    className="display-none question"
                                                    id="information-evento"
                                                >
                                                    <strong>Evento</strong>
                                                    <p>
                                                        Qual a identificação do evento
                                                        criado na central de serviços.
                                                    </p>
                                                </div>
                                            </div>
                                            
                                            <div className="div-label-form-events">
                                                <label className="label-form-color">
                                                    <strong>Conferência</strong>
                                                </label>
                                                <input
                                                    type="text"
                                                    className="form-control input-form-event-conferencia"
                                                    onChange={(event) =>
                                                        setConferencia(
                                                            event.target.value
                                                        )
                                                    }
                                                    placeholder="Por favor descreva sua sala ou local para tratamento da crise."
                                                />
                                                <span>
                                                    <img
                                                        src={iconQuestion}
                                                        alt="question"
                                                        onClick={(event) => {
                                                            if (
                                                                document
                                                                    .getElementById(
                                                                        "information-conferencia"
                                                                    )
                                                                    .classList.contains(
                                                                        "display-none"
                                                                    )
                                                            ) {
                                                                document
                                                                    .getElementById(
                                                                        "information-conferencia"
                                                                    )
                                                                    .classList.remove(
                                                                        "display-none"
                                                                    );
                                                            } else {
                                                                document
                                                                    .getElementById(
                                                                        "information-conferencia"
                                                                    )
                                                                    .classList.add(
                                                                        "display-none"
                                                                    );
                                                            }
                                                        }}
                                                    ></img>
                                                </span>
                                                <div
                                                    className="display-none question"
                                                    id="information-conferencia"
                                                >
                                                    <strong>Conferência</strong>
                                                    <p>
                                                        Descrever um local ou meios de
                                                        comunicação para tratamento da
                                                        crise.
                                                    </p>
                                                </div>
                                            </div>
        
                                            <div className="row">
                                                <div className="mb-5">
                                                    <button
                                                        type="submit"
                                                        className="btn btn-success"
                                                    >
                                                        <b>Adicionar</b>
                                                    </button>
        
                                                    <Link
                                                        to={{
                                                            pathname: "/Dashboard",
                                                        }}
                                                    >
                                                        <button
                                                            type="button"
                                                            className="btn btn-primary"
                                                        >
                                                            <b>Voltar</b>
                                                        </button>
                                                    </Link>
                                                </div>
                                            </div>
                                        </>
                                        )
                                    }
                                </span>
                            </form>
                        </main>
                    </StyledEvents>
                </MenuAppStyled>
            </Styled>
        </>
    );
}
