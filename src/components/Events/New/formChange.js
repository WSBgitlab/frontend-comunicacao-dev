import React, { useState } from "react";

import { Link } from "react-router-dom";

import { cardError, cardInformation } from "../../../config/cards";

import { useHistory } from "react-router-dom";

import api from "../../../config/api";

import iconQuestion from "./../../../assets/icons/questions.png";

export default function ChangeEvents(props) {
    const [change, setChange] = useState("");
    const [description, setDescription] = useState("");
    const [statusChange] = useState("");
    const [started, setStarted] = useState("");
    const [rto] = useState("");

    const history = useHistory();

    async function handleSubmitNewChange(event) {
        event.preventDefault();

        if ((change && description && started) === "") {
            cardError("Preencha os campos corretamente");
            return false;
        }

        try {
            const newEventChange = await api.post(
                "/api/v1/events/changes/create",
                {
                    change,
                    description,
                    statusChange,
                    started,
                    rto,
                    token: localStorage.getItem("keT"),
                },
                {
                    headers: {
                        token: localStorage.getItem("keT"),
                    },
                }
            );

            if (newEventChange.status === 200) {
                cardInformation("Evento Mudança cadastrado com sucesso!");

                // eslint-disable-next-line react/prop-types
                history.push("/dashboard");
            }
        } catch (err) {
            console.log("Erro ao cadastrar", err);
        }
    }

    return (
        <form
            id="form-event"
            className="form-add-event"
            onSubmit={handleSubmitNewChange}
        >
            {
                // Ambiente
            }
            <div className="div-label-form-events">
                <label className="label-form-color" htmlFor="Ambiente">
                    <strong>Mudanças e Avisos</strong>
                </label>
                <select
                    className="form-control"
                    onChange={(event) => setChange(event.target.value)}
                >
                    <option defaultValue="">Selecione</option>
                    <option value="COMUTACAO_LOCAL">COMUTAÇÃO LOCAL</option>
                    <option value="COMUTACAO_REMOTA">COMUTAÇÃO REMOTA</option>
                    <option value="DEPLOY">DEPLOY</option>
                </select>
                    <span>
                        <img
                        src={iconQuestion}
                        alt="question"
                        onClick={(event) => {
                            if (
                                document
                                    .getElementById("information-ambiente")
                                    .classList.contains("display-none")
                            ) {
                                document
                                    .getElementById("information-ambiente")
                                    .classList.remove("display-none");
                            } else {
                                document
                                    .getElementById("information-ambiente")
                                    .classList.add("display-none");
                            }
                        }}
                    ></img>
                    </span>
                <div className="display-none question">
                    <strong>Ambiente</strong>
                    <p>
                        <b style={{ color: "#a5c8ff" }}>[DEPLOY]</b> / ou{" "}
                        <b style={{ color: "#a5c8ff" }}>[COMUTAÇÃO LOCAL]</b> /{" "}
                        ou{" "}
                        <b style={{ color: "#a5c8ff" }}>[COMUTAÇÃO REMOTA]</b>{" "}
                        Tipo da Mudança que será executado.
                    </p>
                    <br></br>
                    <strong>Exemplo :</strong>
                    <p>PRD ou HEXT</p>
                </div>

                <div
                    className="display-none question"
                    id="information-ambiente"
                >
                    <strong>Mudanças e Avisos</strong>
                    <p>
                        Escolha uma opção que seja referente ao ambiente em que
                        o sistema está sendo{" "}
                        <b style={{ color: "#a5c8ff" }}>hospedado</b> /{" "}
                        <b style={{ color: "#a5c8ff" }}>executando</b> .
                    </p>
                </div>
            </div>
            {
                // Mudança
            }
            <div className="div-label-form-events">
                <label className="label-form-color" htmlFor="Statys">
                    <strong>Mudança Descrição</strong>
                </label>
                <input
                    type="text"
                    className="form-control input-form-event-conferencia"
                    onChange={(event) => setDescription(event.target.value)}
                    placeholder="Identificação da mudança criada"
                />
                <span>
                    <img
                        src={iconQuestion}
                        alt="question"
                        onClick={(event) => {
                            if (
                                document
                                    .getElementById("information-change")
                                    .classList.contains("display-none")
                            ) {
                                document
                                    .getElementById("information-change")
                                    .classList.remove("display-none");
                            } else {
                                document
                                    .getElementById("information-change")
                                    .classList.add("display-none");
                            }
                        }}
                    ></img>
                </span>
                <div className="display-none question" id="information-change">
                    <strong>Mudança</strong>
                    <p>
                        Neste campo deve ser informado o número da mudança e a
                        descrição do Resumo da mesma.
                    </p>
                </div>
            </div>
            {
                // Status
            }
            {
                // Início e Fim
            }
            <div className="div-label-form-events">
                <label className="label-form-color" htmlFor="Statys">
                    <strong>Início</strong>
                </label>
                <input
                    type="text"
                    className="form-control input-form-event-conferencia"
                    onChange={(event) => setStarted(event.target.value)}
                    placeholder="00/00/2020 09:00"
                />
                <span>
                    <img
                        src={iconQuestion}
                        alt="question"
                        onClick={(event) => {
                            if (
                                document
                                    .getElementById("information-startend")
                                    .classList.contains("display-none")
                            ) {
                                document
                                    .getElementById("information-startend")
                                    .classList.remove("display-none");
                            } else {
                                document
                                    .getElementById("information-startend")
                                    .classList.add("display-none");
                            }
                        }}
                    ></img>
                </span>
                <div
                    className="display-none question"
                    id="information-startend"
                >
                    <strong>Início</strong>
                    <p>Início da Mudança.</p>
                </div>
            </div>

            <div className="row">
                <div className="mb-5">
                    <button type="submit" className="btn btn btn-primary">
                        <b>Adicionar</b>
                    </button>

                    <Link
                        to={{
                            pathname: "/Dashboard",
                        }}
                    >
                        <button type="button" className="btn btn btn-primary">
                            <b>Voltar</b>
                        </button>
                    </Link>
                </div>
            </div>
        </form>
    );
}
