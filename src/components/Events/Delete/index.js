import React, { useEffect, useState } from "react";

import Api from "./../../config/api";

import { cardError, cardInformation } from "./../../config/cards";

import Container from "./styled.js";

import { useHistory } from "react-router-dom";

import { makeStyles } from '@material-ui/core/styles';

import Table from '@material-ui/core/Table';

import TableBody from '@material-ui/core/TableBody';

import TableCell from '@material-ui/core/TableCell';

import TableContainer from '@material-ui/core/TableContainer';

import TableHead from '@material-ui/core/TableHead';

import TableRow from '@material-ui/core/TableRow';

import Paper from '@material-ui/core/Paper';

export default function Index(props) {
    const [describe, setDescribe] = useState("");

    const [comments, setComments] = useState([]);

    const [countChar, setCountChar] = useState(0);

    const history = useHistory();

    const dateCreated = new Date(props.history.location.state.createdAt).toLocaleString();
    
    const finish = new Date().toLocaleString();

    document.title = `Deletar Evento`;
    
    useEffect(() => {
        setCountChar(describe.length);
    },[describe])


    useEffect(() => {
        async function getComments() {
            Api.post("/api/v1/users/validate/token", {
                token: localStorage.getItem("keT"),
            },{
                headers : {
                    token : localStorage.getItem("keT")
                }
            }).then(async (res) => {
                const response = await Api.get(
                    // eslint-disable-next-line react/prop-types
                    `/api/v1/events/comments/list/${props.history.location.state.id_event}`,
                    {
                        headers: {
                            token: localStorage.getItem("keT"),
                        },
                    }
                );

                setComments(response.data);
            });
        }

        getComments();
    }, []);

    async function handleTelegramSubmit(){
        try {
            Api.post("api/v1/users/validate/token", {
                token: localStorage.getItem("keT"),
            },{
                headers :{
                    token : localStorage.getItem("keT") 
                }
            }).then(async (res) => {
                if (res.data.status === undefined || res.data.status === false) {
                    history.push("/");
                } else {
                    const teamsMessage = await Api.post("/api/v1/bot/send/message/event/teams",
                    {
                        id_messages : 1,
                        text: describe,
                        relevancia : props.location.state.relevancia,
                        evento : props.location.state.evento,
                        servico : props.location.state.sistema,
                        ambiente : props.location.state.ambiente,
                        sintoma : props.location.state.sintoma,
                        status : describe     
                    },
                    {
                        headers: {
                            token: localStorage.getItem("keT")  
                        }, 
                    }); // End post requisition

                    if ((teamsMessage.statusText === "OK")) {
                        cardInformation("Mensagem enviada com sucesso!");

                        return true;
                    } else {
                        cardError("Mensagem não foi enviada com sucesso!");

                        return false; 
                    } 
                }  
            }).catch(erro => console.log(erro, 'erro'))
        } catch (err) {
            cardError(err, "Erro ao enviar mensagem ");
        }
    }

    async function sendEmail() {
        let string = `Acompanhamento de Evento %0A%0AInício : ${dateCreated}  %0AFinalizado : ${finish} %0A%0A`;
        comments.map((item) => {
            let data = new Date(item.updated_at);

            string +=`${item.nome} - ${data.toLocaleString()}:%0A%0A\n ${item.comentario}%0A%0A\n`;
        });

        // Não colocar espaços ou /n/r (ENTER) dentro dessa string ele vai alterar o resultado no telegram!
        // eslint-disable-next-line react/prop-types
        let body = `&body=Sistema%20:%20${props.location.state.sistema}%20-%20${props.location.state.ambiente}%0A%0A
        %0ASintoma%20%20:%20%20${props.location.state.sintoma}%0A%0ADescrição Final%0A${describe}
        %0A%0A%0ATimeline do Evento%0A%0A${string}%0A`;

        document.location.href = `mailto:IncidenteseProblemas@cip-bancos.org.br;daniel.saule@cip-bancos.org.br?subject=${props.location.state.sintoma}%0A${body} %0A%0A%20Atenciosamente.%0A%20Centro%20e%20Comando%0A%20Infraestrutura e Suporte TI%0A%20www.cip-bancos.org.br`;
    }

    async function handleCloseCrise(events) {
        events.preventDefault();

        if (describe.length <= 30) {
            cardError("Por favor, detalhe sua solução.");
            return false;
        }

        try {
            await Api.post("/api/v1/users/validate/token", {
                token: localStorage.getItem("keT"),
            },{
                headers : {
                    token : localStorage.getItem("keT")
                }
            }).then(async (res) => {
                    const responseUser = await Api.post(
                        "/api/v1/users/list/single",
                        {
                            token: localStorage.getItem("keT"),
                        },
                        {
                            headers: {
                                token: localStorage.getItem("keT"),
                            },
                        }
                    );
                    
                    const { nome, sobrenome, atuacao } = responseUser.data;

                    const msg = `${describe} \n\n - ${nome} \n - ${sobrenome} \n - ${atuacao} \n\n `;
                    // eslint-disable-next-line react/prop-types
                    const responseClose = await Api.post(
                        // eslint-disable-next-line react/prop-types
                        `/api/v1/events/delete/${props.match.params.id_event}`,
                        {
                            closeComments: msg,
                            token: localStorage.getItem("keT"),
                        },
                        {
                            headers: {
                                token: localStorage.getItem("keT"),
                            },
                        }
                    );

                    if (responseClose.status === 200) {
                        sendEmail();
                        cardInformation("Evento fechado com sucesso!");
                        // handleTelegramSubmit();
                        // eslint-disable-next-line react/prop-types
                        history.push("/dashboard");
                    } else {
                        cardError("Erro ao excluir evento");
                    }
                })
                .catch((err) => console.log("erro" + err));
        } catch (erro) {
            cardError(
                "Erro inesperado! reload a page ou faça o login novamente!"
            );
        }
    }

    // Material UI
    const useStyles = makeStyles({
        table: {
          minWidth: 650,
        },
      });
      
      function createData(ambiente, conferencia, relevancia, sintoma, servico) {
        return { ambiente, conferencia, relevancia, sintoma, servico };
      }
      
      const rows = [
        createData(
            `${props.location.state.ambiente}`, 
            `${props.location.state.conferencia}`, 
            `${props.location.state.relevancia}`, 
            `${props.location.state.sintoma}`,
            `${props.location.state.servico}`
        )
    ];


    const classes = useStyles();

    return (
        <Container>
            <div className="col col-md-8 box-close-events">
                <header>
                    <div className="back" onClick={() => {
                        // eslint-disable-next-line react/prop-types
                        props.history.push("/dashboard");
                    }}>
                        <span className="material-icons">
                            arrow_back
                        </span>
                        Voltar
                    </div>

                    <h2>Fechar Crise</h2>

                    <hr />
                </header>
                <main>
                    <form id="" onSubmit={handleCloseCrise}>
                    <TableContainer component={Paper}>
                        <Table className={classes.table} aria-label="simple table">
                            <TableHead>
                            <TableRow>
                                <TableCell>Ambiente</TableCell>
                                <TableCell align="left">Conferência</TableCell>
                                <TableCell align="left">Relevância</TableCell>
                                <TableCell align="left">Sintoma</TableCell>
                                <TableCell align="left">Serviço</TableCell>
                            </TableRow>
                            </TableHead>
                            <TableBody>
                            {rows.map((row) => (
                                <TableRow key={row.ambiente}>
                                <TableCell component="th" scope="row">
                                    {row.ambiente}
                                </TableCell>
                                <TableCell align="left">{row.conferencia}</TableCell>
                                <TableCell align="left">{row.relevancia}</TableCell>
                                <TableCell align="left">{row.sintoma}</TableCell>
                                <TableCell align="left">{row.servico}</TableCell>
                                </TableRow>
                            ))}
                            </TableBody>
                        </Table>
                        </TableContainer>
                        
                        <div className="col col-md-12 describe-solution">
                            <header>
                                <h3>Resolução da Crise</h3>
                            </header>

                            <main>
                                <blockquote>
                                    <b>
                                        Descreva sua solução, este campo deve ter no minímo de 30
                                        caracteres.

                                        <b style={{ textAlign : "left"}}>  {countChar}/30</b>
                                    </b>
                                </blockquote>
                                <br></br>
                                <textarea
                                    className="form-control"
                                    placeholder="Descreva sua solução para esse evento de comunicação."
                                    onChange={(event) =>
                                        setDescribe(event.target.value)
                                    }
                                ></textarea>
                            </main>
                        </div>

                        <button
                            type="submit"
                            className="btn btn-outline-danger"
                        >
                            Fechar Crise
                        </button>
                    </form>
                </main>
            </div>
        </Container>
    );
}
