import styled from "styled-components";

const Container = styled.div`
    width: 100%;
    background: #ebebeb;
    display: flex;
    justify-content: center;
    box-sizing: content-box;
    align-items: center;

    

    div.box-close-events {
        background: #fff9f9;
        border-radius: 10px;
        margin: 100px;
        box-shadow: 1px 1px 9px #ccc;
        header {
            div.back {
                display:flex;
                align-items:center;
                margin:1em auto;
                cursor:pointer;
            }
            margin: 0 1.5em;
            padding:1.5rem;
            h2 {
                font-size: 23px;
                margin: 0;
                
            }

            hr {
                width:50%;
                height: 4px;
                display: block;
                border: none;
                margin:0px;
                border-radius: 20px;
                background: #313b6b;
            }
        }

        main {
            display: flex;
            padding: 0 3em;
            text-align: center;

            form {
                width: 100%;

                div.describe-solution {
                    header {
                        display: flex;
                        width: 100%;
                        text-align: left;
                        h3 {
                            font-size: 24px;
                            color: #232323;;
                            border-bottom: 4px solid #313b6b;
                            border-radius: 4px;
                        }
                    }

                    main {
                        display: flex;
                        flex-direction: column;
                        text-align: left;
                        label {
                            color: #313b6b;
                            font-weight: 700;
                        }
                        p {
                            color: #fff;
                        }

                        blockquote{
                            font-size: 15px;
                        }
                        textarea {
                            background: transparent;
                            height: 100px;
                            color: #000;
                        }
                    }
                }
            }
            ul {
                list-style: none;
                list-style: none;
                display: flex;
                justify-content: center;
                flex-direction: column;
                align-items: center;
                background: #ededed;
                box-sizing: content-box;
                li {
                    color:   #06488a;;
                    padding: 0.5em;
                    display: flex;
                    justify-content: space-between;

                    b {
                        color: #313b6b;
                        width: 200px;
                        display: block;
                        float: left;
                        text-align: left;
                        margin: 0 0 0 4em;
                    }
                }
            }

            button {
                margin: 1.5em 0;
            }
        }
    }
`;

export default Container;
