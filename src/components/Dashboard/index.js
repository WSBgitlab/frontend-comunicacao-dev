import React from "react";

import Styled from "./styled";

import Menu from "./../Screens/Menu/index";

import MainAppStyled from "./../Screens/MainApp/styled";

import EventsOpen from "../Events/Open/index";

import EventsClosed from "./../Events/Closed/index";

import EventsChange from "./../ChangesIT/Open/index";

import EventsChangeClosed from "./../ChangesIT/Closed/index";

export default function Dashboard() {

    document.title = "Portal Dashboard"
    
    return (
        <>
            <Menu />
            <Styled>
                <MainAppStyled className="col col-md-10 maingrid-dash">
                    <header>
                        <span>
                            <h3>Portal de Comunicação - Dashboard</h3>
                        </span>
                        <hr></hr>
                    </header>

                    <main>
                        <div className="boxes-events">
                            <div className="main-tables">
                                <EventsOpen  />
                            </div>
                        </div>

                        <div className="boxes-events">
                            <div className="main-tables">
                                <EventsClosed  />
                            </div>
                        </div>

                        <div className="boxes-events">
                            <div className="main-tables">
                                <EventsChange  /> 
                            </div>
                        </div>

                        <div className="boxes-events">
                            <div className="main-tables">
                                <EventsChangeClosed  /> 
                            </div>
                        </div>
                    </main>
                </MainAppStyled>
            </Styled>
        </>
    );
}
