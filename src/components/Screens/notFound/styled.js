import styled from 'styled-components';

const styleNotFound = styled.div`
    width:100%;
    
    background: #51484b;;
    .aviso-container{
        height: 100vh;
        display: flex;
        align-items: center;
    }

`;


export default styleNotFound;