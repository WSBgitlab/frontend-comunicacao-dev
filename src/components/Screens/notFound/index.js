import React from 'react';

import { Container, Modal, Alert } from 'react-bootstrap';

import Styled from './styled';

const NotFound = ({ props }) => {
    return(
        <Styled>
            <Container className="aviso-container">
                <Modal.Dialog>
                    <Modal.Header closeButton>
                        <Modal.Title>Portal de Crise - Aviso</Modal.Title>
                    </Modal.Header>

                    <Modal.Body>

                    <Alert key={2} variant='danger'>
                        <b>
                            Estamos em Manutenção, caso tenha dúvidas entre em contato com time de Tools. 
                            <hr></hr>
                            <p>ferramentaseAutomação@cip-bancos.org.com</p>
                        </b>
                    </Alert>
                    </Modal.Body>
                    </Modal.Dialog>
            </Container>
        </Styled>
    )
}

export default NotFound;