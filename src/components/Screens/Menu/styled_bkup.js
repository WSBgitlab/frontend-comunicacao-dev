import styled from 'styled-components';


const StyledMenu = styled.div`
    div.menu-grid{
            height: 100%;
            padding:0;
            width:150px;
            float:left;
            position:fixed;
            display: flex;
            flex-direction : column;
            z-index: 9999;
            background: #dcdcdcb3;
            border-right: 2px solid #f2f2f2f2;
            box-shadow: 3px 2px 2px #f3f3f3f3;
            
            section{
                height: 100%;
                div.avatar-user{
                    width: 70px;
                    height: 70px;
                    display: block;
                    position: relative;
                    margin: 1em auto 0; 
                    border: 1px solid #ccc;
                    border-radius: 50%;
                    text-align: center;

                    img{
                        width: 100%;
                        height: 100%;
                        border: 1px transparent;
                        border-radius: 50%;
                        margin-bottom: 1em;
                    } 

                    i{
                        font-size: 12px;
                        color: #000;
                        font-weight: 800;
                    }   
                }
            }

            div.menu-aside{
                height:100%;
                nav{   
                    text-align: center;
                    ul{
                        list-style: none;

                        li{
                            width: 84%;
                            margin: 1em auto;
                            button{
                                background-color: #737272;
                                color: #fff;
                                position: relative;
                                width: 85%;

                                &:hover{
                                    bottom: 4px;
                                    box-shadow:0px 10px 5px #c2c2c2c2;    
                                }
                            }
                        }
                    }
                }
            }
        }
`


export default StyledMenu;