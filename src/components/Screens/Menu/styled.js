import styled from 'styled-components';


const StyledMenu = styled.div`
    *{
        margin: 0;
        padding: 0;
        box-sizing: content-box;
    }

    html,body{
        height: 100vh;
        background: rgba(230, 227, 227, 0.774);
    }

    .display-flex-content{
        display: flex !important;
        justify-content: center;
        flex-direction: column;
        align-items: center;
    }

    div.menu-grid{
        width: 48px;
        height: 100%;
        background: #fff;
        z-index: 9999;
        display: flex;
        flex-direction: column;
        align-items: center;
        position: fixed;

        header {
            display: block;
            height: 50px;
            width: 50px;
            padding: 1em 0;
            box-sizing: content-box;

            div.information-active{
                display: none ;
                height: 120px;
                width: 400px;
                position: absolute;
                font-weight: 300;
                background: #130e0ecf;
                color: rgba(255, 255, 255, 0.788);
                top: 5px;
                left: 60px;
                border: 1px transparent;
                z-index: 99999;
                border-radius: 5px;
                font-size: 13px;

                ul{
                    list-style: none;
                    margin:0;
                    width: 90%;
                    li{
                        padding: 0.5em;
                        display: flex;
                        border-bottom:1px solid #ccc;

                        &:last-child{
                            border:none;
                        }

                        b{
                            width: 50px;
                        }

                        b + b {
                            font-weight: 900;
                            color: rgb(204, 221, 50);
                        }
                    }
                }
            }

            section{
                img{
                    width: 100%;
                    border: 1px ;
                    border-radius: 5px;
                    box-shadow:1px 4px 5px #ccc;
                }
            }
        }

        main{
                display: flex;
                justify-content: space-between;
                height: 80%;
                width: 100%;

                nav , ul {
                    height: 100%;
                    width:100%;
                }

                ul {
                    list-style:none;
                    li{
                        width: 100%;
                        height: 30px;
                        margin-top: 1em;
                        text-align: center;
                        transition: all 0.5s;
                        cursor:pointer;
                        &:hover{
                            border-left: 5px solid #276191;
                            background: rgb(247, 245, 245);
                        }
                    }

                    .first:hover::after{
                        content: "Home";
                        width: 50px;
                        display: block;
                        position: relative;
                        top: -19px;
                        font-size: 11px;
                        left: 50px;
                        background: #454647;
                        text-align: center;
                        color: #ffff;
                        font-weight: 600;
                        border: 1px transparent;
                        border-radius: 5px;
                        transition: all 1s;
                    }

                    .add-event:hover::after{
                        content: "Adicionar evento";
                        width: 150px;
                        display: block;
                        position: relative;
                        top: -19px;
                        font-size: 11px;
                        left: 50px;
                        background: #454647;
                        text-align: center;
                        color: #ffff;
                        font-weight: 600;
                        border: 1px transparent;
                        border-radius: 5px;
                        transition: all 1s;
                    }

                    .last:last-of-type:hover::after{
                        content: "Log out";
                        width: 50px;
                        display: block;
                        position: relative;
                        top: -19px;
                        font-size: 11px;
                        left: 50px;
                        background: #454647;
                        text-align: center;
                        color: #ffff;
                        font-weight: 600;
                        border: 1px transparent;
                        border-radius: 5px;
                        transition: all 1s;
                    }
                }
            }

        section{
            width: 40px;
            margin: 0 auto;
            position: relative;
            cursor:pointer;
        }
    }
`


export default StyledMenu;