import React, { useState } from "react";

import Api from "./../../../config/api";

import Container from "./styled.js";

import { useHistory } from "react-router-dom";

import { cardError, cardSucess } from "./../../../config/cards";

export default function Index(props) {
    const [user, setUser] = useState("");
    const [passOld, setPassOld] = useState("");
    const [passNew, setPassNew] = useState("");

    const history = useHistory();

    async function resetPassowordSubmit(e) {
        e.preventDefault();
        if ((user && passOld && passNew) === "") {
            cardError("Preencha os campos corretamente!");
        }

        let result = await Api.put("/api/v1/users/reset/password", {
            login: user,
            pass_new: passNew,
            pass_old: passOld,
        });

        if (result.status === 401)
            cardError("Senha antiga não correspondente!");

        if (result.status === 200) cardSucess("Senha resetada com sucesso!");

        history.push("/");

        return true;
    }

    return (
        <Container>
            <div className="col col-md-8 box-close-events">
                <header>
                    <div
                        className="back"
                        onClick={() => {
                            history.push("/");
                        }}
                    >
                        <span className="material-icons">arrow_back</span>
                        Voltar
                    </div>

                    <h2>Resetar senha</h2>

                    <hr />
                </header>
                <main>
                    <form id="" onSubmit={resetPassowordSubmit}>
                        <div className="col col-md-12 describe-solution">
                            <main>
                                <label>Usuário</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    onChange={(e) => setUser(e.target.value)}
                                />
                                <br></br>
                                <label>Senha Antiga</label>
                                <input
                                    type="password"
                                    className="form-control"
                                    onChange={(e) => setPassOld(e.target.value)}
                                />
                                <br></br>
                                <label>Senha Nova</label>
                                <input
                                    type="password"
                                    className="form-control"
                                    onChange={(e) => setPassNew(e.target.value)}
                                />
                            </main>
                        </div>

                        <button
                            type="submit"
                            className="btn btn-outline-primary"
                        >
                            Resetar Senha
                        </button>
                    </form>
                </main>
            </div>
        </Container>
    );
}
