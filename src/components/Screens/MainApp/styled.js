import styled from 'styled-components';


const MenuAppStyled = styled.div`
    padding: 3em;
    top: 3rem;
    position: relative;
    border-radius: 5px;
    left: 10%;
    float: left;
    margin: 0;
    background: #fff;
    box-sizing: content-box;


    button.btn-form-dark{
        background: #fff;
        color: black;
        border: 1px solid #000;
        transition: color .15s ease-in-out,background-color .15s ease-in-out,border-color .15s ease-in-out,box-shadow .15s ease-in-out;
        &:active{
            color: #fff;
            background-color: #343a40;
            border-color: #343a40;
        }

        &:focus{
            box-shadow: 0 0 0 0.2rem rgba(52,58,64,.5);
        }

        &:hover{
            color: #fff;
            background-color: #343a40;
            border-color: #343a40;
        }
    }
`;

export default MenuAppStyled;