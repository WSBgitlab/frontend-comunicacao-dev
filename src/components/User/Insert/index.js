import React, {  useState } from "react";

import "./../../../css/style";

import Styled from "./styled";

import { useHistory } from 'react-router-dom';

import Api from './../../../config/api';

import { cardError, cardInformation } from './../../../config/cards';

export default function Index(props) {
    const [nome, setNome] = useState("");
    const [password, setPassword] = useState("");
    const [email, setEmail] = useState("");
    const [login, setLogin] = useState("");
    const [role, setRole] = useState("");
    const [setor, setSetor] = useState("");
    const [terceiro, setTerceiro] = useState(false);
    const [atuacao, setAtuacao] = useState("");
    const [empresa, setEmpresa] = useState("");

    const history = useHistory();

    async function handleSubmitNewUser(event) {
        event.preventDefault();

        if (
            nome === "" ||
            password === "" ||
            email === "" ||
            login === "" ||
            setor === "" ||
            terceiro === "" ||
            empresa === ""
        ) {
            cardError("Formulário deve ser preenchido corretamente!");
        } else {
            setRole("Default");
            const response = await Api.post("/api/v1/user/create", {
                nome,
                pass: password,
                email,
                login,
                atuacao,
                role,
                terceiro,
                setor,
                empresa,
            });

            if (response.status === 200) {
                cardInformation("Usuário Inserido com Sucesso!");
                // eslint-disable-next-line react/prop-types
                history.push("/");
            } else {
                cardError("Usuário não cadastrado!");
            }
        }
    }

    return (
        <Styled>
            <div className="col col-md-12 div-grid-geral">
                <header>
                    <h2>Novo Usuário - CIP Portal de Crise </h2>
                </header>
                <form id="id-insert-new-user" onSubmit={handleSubmitNewUser}>
                    <div className="row">
                        <p>Informações de Login</p>
                        <div className="col col-md-10 box-information-login">
                            <label htmlFor="Login">Login</label>
                            <input
                                type="text"
                                className="form-control input-add-user"
                                placeholder="Inserir seu Login"
                                onChange={(event) =>
                                    setLogin(event.target.value)
                                }
                            />

                            <label htmlFor="senha">Senha</label>
                            <input
                                type="password"
                                className="form-control input-add-user"
                                onChange={(event) =>
                                    setPassword(event.target.value)
                                }
                            />

                            <label htmlFor="email">Email</label>
                            <input
                                type="email"
                                className="form-control input-add-user"
                                placeholder="example@cip-bancos.com.br"
                                onChange={(event) =>
                                    setEmail(event.target.value)
                                }
                            />
                        </div>
                    </div>
                    <div className="row">
                        <p>Detalhes do novo Usuário</p>
                        <div className="col col-md-10 box-information-people">
                            <label htmlFor="nome">Nome</label>
                            <input
                                type="text"
                                style={{ textTransform: "capitalize" }}
                                className="form-control input-add-user"
                                onChange={(event) =>
                                    setNome(event.target.value)
                                }
                            />

                            <label htmlFor="setor">Setor</label>
                            <input
                                type="text"
                                className="form-control input-add-user"
                                placeholder="Ex : Engenharia , Tecnologia da Informação"
                                onChange={(event) =>
                                    setSetor(event.target.value)
                                }
                                style={{ textTransform: "capitalize" }}
                            />

                            <label htmlFor="terceiro">Terceiro</label>
                            <select
                                className="form-control input-add-user"
                                onChange={(event) =>
                                    setTerceiro(event.target.value)
                                }
                            >
                                <option defaultValue="2">Selecione</option>
                                <option value="1">Sim</option>
                                <option value="0">Não</option>
                            </select>

                            <label htmlFor="cargo">Cargo</label>
                            <input
                                type="text"
                                className="form-control input-add-user"
                                onChange={(event) =>
                                    setAtuacao(event.target.value)
                                }
                                style={{ textTransform: "capitalize" }}
                            />

                            <label htmlFor="empresa">Empresa</label>
                            <select
                                className="form-control input-add-user"
                                onChange={(event) =>
                                    setEmpresa(event.target.value)
                                }
                            >
                                <option defaultValue="">Selecione</option>
                                <option value="CIP – Câmara Interbancária de Pagamentos.">
                                    CIP – Câmara Interbancária de Pagamentos.
                                </option>
                                <option value="TIVIT">TIVIT</option>
                            </select>
                        </div>

                        <button className="btn btn-outline-dark" type="submit">
                            Cadastrar
                        </button>
                    </div>
                </form>
            </div>
        </Styled>
    );
}
