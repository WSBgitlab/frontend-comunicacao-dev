import React from "react";

import { useSelector, useDispatch } from "react-redux";

export default function Index() {
    const { event, activeEvent } = useSelector((state) => state.events);
    const dispatch = useDispatch();

    // Action Adicionar evento
    function addEvent() {
        dispatch({
            type: "ACTIVE_EVENT",
            payload: {
                id: 2,
                causa: "Servidor com teste",
            },
        });
    }

    return (
        <>
            <h1>Events</h1>
            {event.map((data) => (
                <div key={data.id}>
                    <h4>{data.name}</h4>
                    <p>{data.causa}</p>
                </div>
            ))}
            <h2>Events Ongoing</h2>
            <strong>{activeEvent.causa}</strong>
            <button type="button" onClick={addEvent}>
                Adicionar Event
            </button>
        </>
    );
}
