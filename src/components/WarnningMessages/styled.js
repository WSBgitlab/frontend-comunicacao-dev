import Styled from 'styled-components';

const Container = Styled.div`
    div.warnning{
        position:absolute;
        width:100%;
        height:100%;
        background-color:#0009;
        z-index:99999;
        align-items:center;
        display:flex;
        position:fixed;

    }    
    div.grid-warnning{
        width: 95%;
        margin: 0 auto;
        background: #f4f5ff;
    }
    div.container-warnning{
        margin: 1.5rem auto;
        background-color:#fff;
        span {
                h3 {
                    margin: 0;
                    font-size: 23px;
                }
            }

            hr {
                width: 50%;
                height: 4px;
                border: 1px solid #313b6b;
                border-radius: 10px;
                background-color: #313b6b;
            }
    }

    table.table-warnning{

    }

`

export default Container;