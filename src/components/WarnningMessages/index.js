import React, { useEffect, useState } from 'react';

import api from 'axios';

import { Spinner, Table, Container , Button } from 'react-bootstrap'

import Styled from './styled';

export default function Index({ stateWarnning }){
    const [load, setLoad] = useState(false);
    
    const [user, setUser] = useState([])
    
    useEffect(() => {
        async function getUsers(){
            setLoad(true)
            const result = await api.get("https://api.github.com/users/SecurityAnalystWB");
            setLoad(false)

            console.log(result.data);

        }

        getUsers();
    }, [window.location.href])
    
    return (
        <>
            <Styled>
                <div className="warnning">
                    { load === true ? <Spinner animation="border" variant="primary" /> : false }
                    
                    <div className="grid-warnning">
                        <Container className="container-warnning">
                        <span>
                            <h3>Avisos Importantes!</h3>
                        </span>
                        <p>Por favor, priorizar os eventos para relatar a sua equipe o progresso do evento!</p>
                        <hr></hr>
                            <Table className="table-container-warnning" responsive="sm">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Table heading</th>
                                    <th>Table heading</th>
                                    <th>Table heading</th>
                                    <th>Table heading</th>
                                    <th>Table heading</th>
                                    <th>Table heading</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>1</td>
                                    <td>Table cell</td>
                                    <td>Table cell</td>
                                    <td>Table cell</td>
                                    <td>Table cell</td>
                                    <td>Table cell</td>
                                    <td>Table cell</td>
                                </tr>
                                <tr>
                                    <td>2</td>
                                    <td>Table cell</td>
                                    <td>Table cell</td>
                                    <td>Table cell</td>
                                    <td>Table cell</td>
                                    <td>Table cell</td>
                                    <td>Table cell</td>
                                </tr>
                                <tr>
                                    <td>3</td>
                                    <td>Table cell</td>
                                    <td>Table cell</td>
                                    <td>Table cell</td>
                                    <td>Table cell</td>
                                    <td>Table cell</td>
                                    <td>Table cell</td>
                                </tr>
                                </tbody>
                            </Table>
                            <Button variant="outline-primary">ok! Irei priorizar.</Button>{' '}
                        </Container>
                    </div>
                </div>
            </Styled>
        </>
    )
}