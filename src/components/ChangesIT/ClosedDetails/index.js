/* eslint-disable react/prop-types */
import React, { useEffect, useState } from "react";

import Container from "../../Events/Delete/styled";

import Api from "../../../config/api";

export default function Index(props) {
    const [detailsEvent, setDetailsEvent] = useState([]);
    const [msgDetails, setMsgDetails] = useState("");

    useEffect(() => {
        async function getDetailsTheEvent() {
            // eslint-disable-next-line react/prop-types
            const response = await Api.get(
                `/api/v1/events/comments/changes/list/${props.match.params.id_change}`,
                {
                    headers: {
                        token: localStorage.getItem("keT"),
                    },
                }
            );

            setDetailsEvent(response.data);
            setMsgDetails(response.data.closeddescribe);
        }

        console.log(detailsEvent);
        getDetailsTheEvent();
    }, [props.match.params.idEvent, detailsEvent]);

    const string = msgDetails.split("-");

    const comentario = string[0];
    const nome = string[1];
    const sobrenome = string[2];
    const atuacao = string[3];

    return (
        <Container>
            <div
                className="back"
                onClick={() => props.history.push("/dashboard")}
            >
                &larr;
            </div>
            <div className="col col-md-8 box-close-events">
                <header>
                    <h2>Status : Fechado</h2>
                    <hr />
                </header>
                <main>
                    <form id="">
                        <ul>
                            <li>
                                <b>Ambiente</b>:
                                <b>{props.history.location.state.ambiente}</b>
                            </li>
                            <li>
                                <b>Conferencia</b>:
                                <b>
                                    {props.history.location.state.conferencia}
                                </b>
                            </li>
                            <li>
                                <b>Relevância</b>:
                                <b>{props.history.location.state.relevancia}</b>
                            </li>
                            <li>
                                <b>Sintoma</b>:
                                <b>{props.history.location.state.sintoma}</b>
                            </li>
                            <li>
                                <b>Sistema</b>:
                                <b>{props.history.location.state.sistema}</b>
                            </li>
                        </ul>

                        <div className="col col-md-12 describe-solution">
                            <header>
                                <h3>Solução</h3>
                            </header>

                            <main>
                                <p>
                                    {`${comentario}`}
                                    <p>
                                        {nome}
                                        {sobrenome} - {atuacao}
                                    </p>
                                </p>
                            </main>
                        </div>
                    </form>
                </main>
            </div>
        </Container>
    );
}
