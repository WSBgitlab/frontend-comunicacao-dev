import React, { useEffect, useState } from "react";

import Api from "../../../config/api";

import { Pagination } from '@material-ui/lab'


import Mudanca from './content';

// eslint-disable-next-line react/prop-types
const MudancaOpen = () => {
    const [changeOpen, setchangeOpen] = useState([]);
    const [loading, setLoad] = useState(false);

    
    const [currentPage, setCurrentPage] = useState(1);
    const [eventsPerPages] = useState(5);
    
    // Obtendo os Events atuais
    const indexOfLastEvent = currentPage * eventsPerPages;
    const indexOfFirstEvent = indexOfLastEvent - eventsPerPages;
    const currentEvent = changeOpen.slice(indexOfFirstEvent, indexOfLastEvent);
    
    // Mudanças Open
    useEffect(() => {
        async function loadEventsChangeOpen() {
            setLoad(true)
            let changeOpen = await Api.get(
                "/api/v1/events/changes/list/open",
                {
                    headers : {
                        token : localStorage.getItem("keT")
                    }    
                
                }
            );
            setLoad(false)
            setchangeOpen(changeOpen.data);
        }

        loadEventsChangeOpen();
    },[]);

    return (
        <>
            
            <Mudanca 
                mudancas={currentEvent} 
                loading={loading}
            />

            {/* <span>{Math.ceil(changeOpen.length / eventsPerPages)}</span> */}
            
            <div className="col col-md-12 pagination-grid">
                <Pagination count={Math.ceil(changeOpen.length / eventsPerPages)} onChange={(event , page) => {
                    event.preventDefault();
                    setCurrentPage(page)
                }}/>
            </div>
        </>
    );
};

export default MudancaOpen;
