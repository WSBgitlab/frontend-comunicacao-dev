import React from 'react'

import { formatDate } from '../../../utils/dateFormats';

import { useHistory } from 'react-router-dom';

import { 
    Button,
    Spinner
} from 'react-bootstrap';

import { makeStyles } from '@material-ui/core/styles';

import Table from '@material-ui/core/Table';

import TableBody from '@material-ui/core/TableBody';

import TableCell from '@material-ui/core/TableCell';

import TableContainer from '@material-ui/core/TableContainer';

import TableHead from '@material-ui/core/TableHead';

import TableRow from '@material-ui/core/TableRow';

import Paper from '@material-ui/core/Paper';

import Container from '@material-ui/core/Container';

const useStyles = makeStyles({
    table: {
        minWidth: 650,
    },
});

const EventsOpen = ({ mudancas , loading}) => {

    const history = useHistory();

    const classes = useStyles();

    if(loading) return <Button variant="primary" disabled>
        <Spinner
            as="span"
            animation="grow"
            size="sm"
            role="status"
            aria-hidden="true"
        />
        Carregando ...
    </Button>

    return (
        <>
            {
                mudancas.length === 0 ? (<Container style={{ textAlign : "center"}}>
                    <h4>Não há Mudanças abertas!</h4>
                </Container>) : (
                <TableContainer style={{ height : "400px"}}component={Paper}>
                    <Table className={classes.table} aria-label="simple table">
                        <TableHead>
                        <TableRow 
                            style={{ background : "rgb(149, 152, 169)"}}
                        >
                            <TableCell className="tr-header" >Tipo de Evento</TableCell>
                            <TableCell className="tr-header" align="left">Descrição</TableCell>
                            <TableCell className="tr-header" align="left">Status</TableCell>
                            <TableCell className="tr-header" align="left">Iniciado</TableCell>
                            <TableCell className="tr-header" align="left">Última Modificação</TableCell>
                        </TableRow>
                        </TableHead>
                        <TableBody>
                        {mudancas.map((data) => (
                            <TableRow 
                                key={data.status}
                                onClick={() => {
                                    history.push(`/event/change/ongoing/${data.id}/${data.fk_users}`,{
                                        id_events_changes : data.id,
                                        change : data.type_change,
                                        description : data.description_change,
                                        started : data.started
                                    })
                                }}
                            >
                                <TableCell component="th" scope="row">{data.updatedAt}</TableCell>
                                <TableCell align="left">{data.description_change}</TableCell>
                                <TableCell align="left">{data.status}</TableCell>
                                <TableCell align="left">{data.started}</TableCell>
                                <TableCell align="left">{formatDate(data.updatedAt)}</TableCell>
                            </TableRow>
                        ))}
                        </TableBody>
                    </Table>
                </TableContainer>
                )
            }
        </>
    )
}

export default EventsOpen;