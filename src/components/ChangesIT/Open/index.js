import React from "react";

import SucessTitle from "./../../../assets/icons/open.png";

import ChangeOpen from './events';

export default function ChangesOpen() {
    return (
        <>
            <div className="header-tables">
                <h3>
                    Mudanças Abertas
                    <img src={SucessTitle} alt=""></img>
                </h3>
                <hr />
            </div>
            <div className="tables">
                <ChangeOpen></ChangeOpen>  
            </div>
        </>
    );
}
