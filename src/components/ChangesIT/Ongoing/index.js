/* eslint-disable react/prop-types */
import React, { useState, useEffect } from "react";

import { cardInformation, cardError } from "../../../config/cards";

import api from "../../../config/api";

import { getToken } from "../../../config/auth";

// import * as configURL from "./../../Bots/Telegram/config";

import { useHistory } from "react-router-dom";

import Menu from "../../Screens/Menu/index";

import io from "socket.io-client";

import MainAppStyled from "../../Screens/MainApp/styled";

import StyledDashboard from "../../Dashboard/styled";

import StyledOngoingEvents from "../../Events/Ongoing/styled";

import iconSendMessage from "./../../../assets/icons/send_comment.png";

import { formatDate } from "../../../utils/dateFormats";

export default function Index(props) {
    const state = props.location.state;

    let urlSocketIO = "http://localhost:8080/";

    if(process.env.NODE_ENV === "production"){
        urlSocketIO = "https://portaldecomunicacao.cip.lan.com";
    }

    const [handleComponent, setHandleComponent] = useState(4);
    
    const [msgTeams,setMsgTeams] = useState("");

    const [lenCharComments, setLenCharComments] = useState(0);
    
    const [idChanges, setIDchanges] = useState(0);

    const [idUsers, setIDusers] = useState(0);

    const socket = io.connect(urlSocketIO,{
        transports : ['websocket'],
        upgrade : false,
        rejectUnauthorized:   false,
    })

    const history = useHistory();

    
    //Comentários que o usuário inserir e será enviados na timeline
    const [sendComment, setSendComment] = useState("");
    
    const [comments, setComments] = useState([]);
    
    const change = props.location.state.change;
    
    const description = props.location.state.description;
    
    const started = props.location.state.started;


    useEffect(() => {
        setIDchanges(props.location.state.id_events_changes);
        setIDusers(localStorage.getItem("upid"));
    },[idChanges])

    useEffect(() => {
        async function changeLength() {
            setLenCharComments(sendComment.length);
        }

        changeLength();
    },[sendComment]);
    
    useEffect(() =>{
        document.querySelector("section.comments").scrollTop = 500000; 
    });

    useEffect(() => {
        async function getCommentsChanges() {
            const response = await api.get(`/api/v1/events/comments/changes/list/${idChanges}`,
                {
                    headers: {
                        token: localStorage.getItem("keT"),
                    },
                }
            );

            setComments(response.data);
        }
        getCommentsChanges();
    }, [handleComponent]);

    socket.on("CommentsChanges", (newComments) => {
        setHandleComponent(handleComponent + 1);

        setComments([...comments, newComments]);

        socket.close();
    });

    async function handleSubmit(e) {
        e.preventDefault();

        const token = localStorage.getItem("keT");

        if (sendComment === "") {
            return cardError("Insira o comentário!");
        } else {
            let obj = {
                token,
                id_users: idUsers, 
                id_events_changes : idChanges, 
                comments: sendComment,
            };

            const response = await api.post("/api/v1/events/change/insert/comments",
                obj,
                {
                    headers: {
                        token,
                    },
                }
            );
                
            if (response.status === 200) {
                cardInformation("Comentário Inserido!");
                document.querySelector("section.comments").scrollTop = 500000;
                setSendComment("");
                // Apagar string
                document.getElementById("textSendComment").value = "";
                // Zerar o text area, somente a variável mas não a parte visual.
                setHandleComponent(handleComponent + 2);
            } else {
                cardError("Não foi possível inserir comentário");
            }
        }
    }
    
    async function handleTeamsSubmit(event) {
        event.preventDefault();

        if(msgTeams == ""){
            cardError("Ops! comentário vazio");
        }

        await api.post("api/v1/users/validate/token", {
            token: getToken(),
        },{
            headers : {
                token : localStorage.getItem("keT")
            }
        })
        .then(async (res) => {
            if (res.data.status === undefined || res.data.status === false) {
                history.push("/");
            } else {
                const teamsMessage = await api.post("/api/v1/bot/send/message/change/teams",
                    {
                        id_messages : 4,
                        change,
                        description,
                        started,
                        text: msgTeams
                    },
                    {
                        headers : {
                            token : localStorage.getItem("keT"),
                            "Content-Type" : "application/json"
                        }
                    }
                );
                

                if ((teamsMessage.statusText = "OK")) {
                    cardInformation("Mensagem enviada com sucesso!");
                    document.getElementById("comments").value = "";
                    setMsgTeams("");
                    return true;
                } else {
                    cardError("Mensagem não foi enviada com sucesso!");
                    document.getElementById("comments").value = "";
                    setMsgTeams("");
                    return false;
                }
            }
        });
    }


    return (
        <>
            <Menu />
            <StyledDashboard>
                <MainAppStyled className="col col-md-10 maingrid-dash">
                    <StyledOngoingEvents>
                        <div className="header">
                            <header>
                                <span>
                                    <h3>Acompanhamento de Mudanças</h3>
                                </span>
                                <hr></hr>
                            </header>

                            <main>
                                <div className="col col-md-12 header-boxes">
                                    <div className="container">
                                        <button type="button" className="btn btn-outline-danger" onClick={(e) => {
                                            console.log(props);
                                            history.push(`/delete/changes/${props.location.state.id_events_changes}`,{
                                                change : props.location.state.change,
                                                description : props.location.state.description,
                                                id : props.location.state.id_events_changes,
                                                started : props.location.state.started
                                            });
                                        }}>Fechar Mudança </button>
                                    
                                    </div>

                                    <div className="header-main-boxes">
                                        <h3>Mudança : {state.change}</h3>
                                        <hr />
                                    </div>
                                    <div className="col col-md-12 main-grid-box">
                                        <div className="col col-md-5 box box-crise">
                                            <header className="box-header">
                                                <b>Informações de Criação</b>
                                            </header>
                                            <nav>
                                                <ul>
                                                    <li>
                                                        <b>Início</b>:
                                                        <i className="ml-5">
                                                            {state.started}
                                                        </i>
                                                    </li>
                                                </ul>
                                            </nav>
                                        </div>
                                    </div>
                                </div>

                                <div className="header-main-box">
                                    <div className="header-main-boxes">
                                        <h3>Detalhes :</h3>
                                        <hr></hr>
                                    </div>

                                    <div className="col col-md-12 main-grid-box timeline">
                                        <div
                                            className="col col-md-8 box box-event box-comments"
                                            style={{
                                                padding: "0px",
                                                height: "358px",
                                                overflow: "auto",
                                                background: "#b6b6b533",
                                            }}
                                        >
                                            <div className="teste"></div>

                                            <section className="comments">
                                                {comments.map((item) => (
                                                    <>
                                                        <div key={item.id} className="comments text-break">
                                                            <div className="header-comments">
                                                                <b>{item.nome}</b> - <b>{formatDate(item.updated_at)}</b>
                                                            </div>
                                                            <div className="main-comments">
                                                                <p>
                                                                    {
                                                                        item.comments
                                                                    }
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </>
                                                ))}
                                            </section>
                                        </div>

                                        <div className="col col-md-8 send-comments">
                                            <form onSubmit={handleSubmit}>
                                                <input
                                                    className="form-control"
                                                    onChantype="text"
                                                    id="textSendComment"
                                                    onChange={e => setSendComment(e.target.value)}
                                                />
                                                <div
                                                    className="input"
                                                    onClick={handleSubmit}
                                                >
                                                    <b>{lenCharComments} / 10</b>
                                                    <img
                                                        src={iconSendMessage}
                                                        alt=""
                                                    ></img>
                                                    <input type="hidden" />
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>

                                <div className="col col-md-12">
                                    <div className="header-main-boxes">
                                        <h3>Enviar mensagem para Teams</h3>
                                        <hr></hr>
                                    </div>

                                    <form
                                        id=""
                                        style={{
                                            width: "100%",
                                            display: "flex",
                                            flexDirection: "column",
                                            alignItems: "center",
                                            justifyContent: "center",
                                        }}
                                        onSubmit={handleTeamsSubmit}
                                    >
                                        <textarea
                                            className="form-control"
                                            style={{
                                                width: "60%",
                                                height: "100px",
                                                display: "flex",
                                                margin: "1em 2em",
                                            }}
                                            id="comments"
                                            onChange={e => setMsgTeams(e.target.value)}
                                        ></textarea>

                                        <button
                                            type="submit"
                                            className="btn btn-outline-info"
                                            style={{
                                                width: "150px",
                                                margin: "0 auto",
                                            }}
                                        >
                                            Enviar{" "}
                                        </button>
                                        <br></br>
                                        <br></br>
                                        <br></br>
                                    </form>
                                </div>
                            </main>
                        </div>
                    </StyledOngoingEvents>
                </MainAppStyled>
            </StyledDashboard>     
        </>
    );
}