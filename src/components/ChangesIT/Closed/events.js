import React, { useEffect, useState } from "react";

import Api from "../../../config/api";

import { Pagination } from '@material-ui/lab'


import Mudanca from './content';

// eslint-disable-next-line react/prop-types
const MudancaClose = () => {
    const [changeClose, setchangeClose] = useState([]);
    const [loading, setLoad] = useState(false);

    
    const [currentPage, setCurrentPage] = useState(1);
    const [eventsPerPages] = useState(5);
    
    // Obtendo os Events atuais
    const indexOfLastEvent = currentPage * eventsPerPages;
    const indexOfFirstEvent = indexOfLastEvent - eventsPerPages;
    const currentEvent = changeClose.slice(indexOfFirstEvent, indexOfLastEvent);
    
    // Mudanças Fechadas
    useEffect(() => {
        async function loadEventsChangeClose() {
            setLoad(true)
            let changeClose = await Api.get(
                "/api/v1/events/changes/list/close",
                {
                    headers : {
                        token : localStorage.getItem("keT")
                    }    
                
                }
            );
            setLoad(false)
            setchangeClose(changeClose.data);
        }

        loadEventsChangeClose();
    },[]);

    return (
        <>
            
            <Mudanca 
                mudancas={currentEvent} 
                loading={loading}
            />

            {/* <span>{Math.ceil(changeClose.length / eventsPerPages)}</span> */}
            
            <div className="col col-md-12 pagination-grid">
                <Pagination count={Math.ceil(changeClose.length / eventsPerPages)} onChange={(event , page) => {
                    event.preventDefault();
                    setCurrentPage(page)
                }}/>
            </div>
        </>
    );
};

export default MudancaClose;
