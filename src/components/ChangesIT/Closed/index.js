import React from "react";

import { logo } from "../../../assets/index";

import ChangeClosed from './events';

export default function ChangeClose() {
    return (
        <>
            <div className="header-tables">
                <h3>
                    Mudanças Fechadas
                    {/* <img src={CloseTitle} alt=""></img> */}
                    <img src={logo} alt=""></img>
                </h3>
                <hr />
            </div>
            <div className="tables">
                <ChangeClosed></ChangeClosed>  
            </div>
        </>
    );
}
