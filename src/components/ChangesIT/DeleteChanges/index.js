/* eslint-disable react/prop-types */
import React, { useEffect, useState } from "react";

import Api from "../../../config/api";

import { cardError, cardInformation } from "../../../config/cards";

import Container from "./styled.js";

import { useHistory } from "react-router-dom";

import { makeStyles } from "@material-ui/core/styles";

import Table from "@material-ui/core/Table";

import TableBody from "@material-ui/core/TableBody";

import TableCell from "@material-ui/core/TableCell";

import TableContainer from "@material-ui/core/TableContainer";

import TableHead from "@material-ui/core/TableHead";

import TableRow from "@material-ui/core/TableRow";

import Paper from "@material-ui/core/Paper";

export default function Index(props) {
    const [describe, setDescribe] = useState("");

    const [comments, setComments] = useState([]);

    const [countChar, setCountChar] = useState(0);

    const history = useHistory();

    document.title = `Deletar Mudança`;

    useEffect(() => {
        async function getComments() {
            Api.post(
                "/api/v1/users/validate/token",
                {
                    token: localStorage.getItem("keT"),
                },
                {
                    headers: {
                        token: localStorage.getItem("keT"),
                    },
                }
            ).then(async (res) => {
                const response = await Api.get(
                    // eslint-disable-next-line react/prop-types
                    `/api/v1/events/comments/list/${props.history.location.state.id_change}`,
                    {
                        headers: {
                            token: localStorage.getItem("keT"),
                        },
                    }
                );
                setComments(response.data);
            });
        }

        getComments();
    }, []);

    useEffect(() => {
        setCountChar(describe.length);
    }, [describe]);

    async function handleTelegramSubmit() {
        try {
            await Api.post(
                "api/v1/users/validate/token",
                {
                    token: localStorage.getItem("keT"),
                },
                {
                    headers: {
                        token: localStorage.getItem("keT"),
                    },
                }
            ).then(async (res) => {
                if (
                    res.data.status === undefined ||
                    res.data.status === false
                ) {
                    history.push("/");
                } else {
                    const teamsMessage = await Api.post(
                        "/api/v1/bot/send/message/change/teams",
                        {
                            id_messages: 5,
                            // eslint-disable-next-line react/prop-types
                            change: props.location.state.change,
                            // eslint-disable-next-line react/prop-types
                            description: props.location.state.description,
                            // eslint-disable-next-line react/prop-types
                            started: props.location.state.started,
                            text: describe,
                        },
                        {
                            headers: {
                                token: localStorage.getItem("keT"),
                            },
                        }
                    );

                    if (teamsMessage.statusCodeUserBot === true) {
                        cardInformation("Mensagem enviada com sucesso!");
                        return true;
                    } else {
                        cardError("Mensagem não foi enviada com sucesso!");
                        return false;
                    }
                } // END ELSE
            });
        } catch (err) {
            cardError(err, "Erro ao enviar mensagem ");
        }
    }

    async function sendEmail() {
        let string = "";

        comments.map((item) => {
            let data = new Date(item.updated_at);

            string += `${item.nome} ${
                item.sobrenome
            } - ${data.toLocaleString()}%0A ${item.comentario}%0A%0A`;
        });

        // Não colocar espaços ou /n/r (ENTER) dentro dessa string ele vai alterar o resultado no telegram!
        // eslint-disable-next-line react/prop-types
        let body = `&body=Mudança%20:%20${props.location.state.change}%20%0A%0A
        %0ADescrição%20${props.location.state.description}%20${props.location.state.started}%0A%0ADescrição Final%0A${describe}
        %0A%0A%0ATimeline do Evento%0A%0A${string}%0A`;

        document.location.href = `mailto:IncidenteseProblemas@cip-bancos.org.br;daniel.saule@cip-bancos.org.br?subject=${props.location.state.sintoma}%0A%0A ${body} %0A%0A%20Atenciosamente.%0A%20Centro%20e%20Comando%0A%20Infraestrutura e Suporte TI%0A%20www.cip-bancos.org.br`;
    }

    async function handleCloseCrise(events) {
        events.preventDefault();

        if (describe.length <= 30) {
            cardError("Por favor, detalhe sua solução.");
            return false;
        }

        try {
            await Api.post(
                "/api/v1/users/validate/token",
                {
                    token: localStorage.getItem("keT"),
                },
                {
                    headers: {
                        token: localStorage.getItem("keT"),
                    },
                }
            )
                .then(async (res) => {
                    const responseUser = await Api.post(
                        "/api/v1/users/list/single",
                        {
                            token: localStorage.getItem("keT"),
                        },
                        {
                            headers: {
                                token: localStorage.getItem("keT"),
                            },
                        }
                    );

                    const { nome, sobrenome, atuacao } = responseUser.data;

                    const msg = `${describe} \n\n - ${nome} \n - ${sobrenome} \n - ${atuacao} \n\n `;
                    // eslint-disable-next-line react/prop-types
                    const responseClose = await Api.post(
                        // eslint-disable-next-line react/prop-types
                        `/api/v1/events/delete/change/${props.match.params.id_change}`,
                        {
                            closeComments: msg,
                            token: localStorage.getItem("keT"),
                        },
                        {
                            headers: {
                                token: localStorage.getItem("keT"),
                            },
                        }
                    );

                    if (responseClose.status === 200) {
                        sendEmail();
                        cardInformation("Evento fechado com sucesso!");
                        handleTelegramSubmit();
                        // eslint-disable-next-line react/prop-types
                        history.push("/dashboard");
                    } else {
                        cardError("Erro ao excluir evento");
                    }
                })
                .catch((err) => console.log(err));
        } catch (erro) {
            cardError(
                "Erro inesperado! reload a page ou faça o login novamente!"
            );
        }
    }

    // Material UI
    const useStyles = makeStyles({
        table: {
            minWidth: 650,
        },
    });

    function createData(change, description, started) {
        return { change, description, started };
    }

    const rows = [
        createData(
            `${props.location.state.change}`,
            `${props.location.state.description}`,
            `${props.location.state.started}`
        ),
    ];

    const classes = useStyles();

    return (
        <Container>
            <div className="col col-md-8 box-close-events">
                <header>
                    <div
                        className="back"
                        onClick={() => {
                            // eslint-disable-next-line react/prop-types
                            props.history.push("/dashboard");
                        }}
                    >
                        <span className="material-icons">arrow_back</span>
                        Voltar
                    </div>

                    <h2>Fechar Crise</h2>

                    <hr />
                </header>
                <main>
                    <form id="" onSubmit={handleCloseCrise}>
                        <TableContainer component={Paper}>
                            <Table
                                className={classes.table}
                                aria-label="simple table"
                            >
                                <TableHead>
                                    <TableRow>
                                        <TableCell>Mudança</TableCell>
                                        <TableCell align="left">
                                            Descrição
                                        </TableCell>
                                        <TableCell align="left">
                                            Iniciado
                                        </TableCell>
                                    </TableRow>
                                </TableHead>
                                <TableBody>
                                    {rows.map((row) => (
                                        <TableRow key={row.ambiente}>
                                            <TableCell align="left">
                                                {row.change}
                                            </TableCell>
                                            <TableCell align="left">
                                                {row.description}
                                            </TableCell>
                                            <TableCell align="left">
                                                {row.started}
                                            </TableCell>
                                        </TableRow>
                                    ))}
                                </TableBody>
                            </Table>
                        </TableContainer>

                        <div className="col col-md-12 describe-solution">
                            <header>
                                <h3>Resolução da Mudança</h3>
                            </header>

                            <main>
                                <blockquote>
                                    <b>
                                        Descreva sua solução, este campo deve
                                        ter no minímo de 30 caracteres.
                                        <b style={{ textAlign: "left" }}>
                                            {" "}
                                            {countChar}/30
                                        </b>
                                    </b>
                                </blockquote>
                                <br></br>
                                <textarea
                                    className="form-control"
                                    placeholder="Descreva sua solução para esse evento de comunicação."
                                    onChange={(event) =>
                                        setDescribe(event.target.value)
                                    }
                                ></textarea>
                            </main>
                        </div>

                        <button
                            type="submit"
                            className="btn btn-outline-danger"
                        >
                            Fechar Crise
                        </button>
                    </form>
                </main>
            </div>
        </Container>
    );
}
