import axios from "axios";

import https from "https";

const headers = {
    "Content-Type": "application/json",
    "Access-Control-Allow-Origin": "*",
    "X-XSS-Protection": "1"
};

let url = "http://localhost:8080";

if(process.env.NODE_ENV === "production"){
    url = "https://portaldecomunicacao.cip.lan.com"
}
 

const api = axios.create({
    baseURL: url,
    httpsAgent: new https.Agent({
        rejectUnauthorized: false,
    }),
    headers,
});

export default api;
