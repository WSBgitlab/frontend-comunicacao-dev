
// Requerindo o token no localstorage com verificação se está preenchido na apliação.
export const isAuthenticated = () => {
    return localStorage.getItem('keT') 
}
// Atribuindo Token.
export const getToken = () => {
  return localStorage.getItem('keT');
}
// Setando o token na aplicação.
export const login = token => {
  localStorage.setItem('keT', token);
};

