import Styled from 'styled-components';

const Container = Styled.div`
    height:100%;
    width: 100%;
    *{
        margin: 0;
        padding: 0;
        box-sizing: content-box;
        font-family: 'Montserrat', sans-serif;
    }

    html,body{
        height: 100vh;
        background: rgba(247, 245, 245, 0.774);
    }


    div.grid-login{
        height: 100%;
        display: flex;
        justify-content: center;
        align-items: center;
        padding: 0px;

        div.aside-login{
            height: 100%;
            width: 70%;
            display: flex;
            flex-direction: column;
            align-items: center;
            float: left;
            font-size: 13px;
            padding-left: 3em;
            justify-content: center;
            box-shadow: 4px 4px 7px #CCD;


            header {
                width: 50%;
            }

            h1{
                font-size: 24px;
                font-weight: 900;
            }

            hr{
                width: 97%;
                height: 3px;
                background-color: #1d2134;
                margin: 1em 0 0.5em 0;
                border: 8px transparent;
                border-radius: 50px;
            }

            input{
                width: 400px;
                height: 16px;
                margin: 10px 0;

                &:active , &:focus {
                    color: #0c0d0d !important;
                    background-color: #fff !important;
                    border-color: #1d213459 !important;
                    outline: 0 !important;
                    box-shadow: 0 0 0 0.2rem rgba(29, 33, 52, 0.84) !important;
                }
            }

            button{
                width: 42%;
                margin: 3em auto;
                display: block;
            }
        }
    }

    .box-main-formulario{
        height: 90%;
        padding: 0;
    }

    .image-background{
        background-image: url('./negocios_background.jpg');
        background-repeat: no-repeat;
        background-size:cover ;
        background-position: center;
        width: 25.2%;
        float: left;
    height: 100%;
    }

    .box-aside-left{
        position: fixed;
        background: #161a30;
        height: 100%;
        width: 30%;
        color: #fff;
    }

    .box-aside-left-information{
        position: fixed;
        background: #4042472a;
        height: 100%;
        width: 30%;
        z-index: 999999;
        color: #fff;
        display: flex;
        flex-direction: column;
        justify-content: center;

        h1{
            font-size: 25px;
<<<<<<< HEAD
        }   
=======
        }
>>>>>>> 299419b1b7b3a95199367291177e1ccf6bbb06a2

        hr{
            width: 97%;
            height: 3px;
            background-color: white;
            margin: 1em 0 0.5em 0;
            border: 8px transparent;
            border-radius: 50px;
        }
    }

    div.opacit{
        height: 100%;
        width: 100%;
        background-color:#161a309f ;
    }


    div.box-aside-left{
        
        > div{
            height: 100%;
            width: 100%;
            color: #fff;
            display: flex;
            align-items: center;
            flex-direction: column;
            justify-content: center;
        }
    }
`;

export default Container;