import React, { useState, useEffect } from "react";

import { Link } from "react-router-dom";

import api from "../../config/api";

import { cardSucess, cardError } from "../../config/cards";

import { useHistory } from "react-router-dom";

import { Button , Spinner } from 'react-bootstrap';

import "./../../css/login.css";

export default function Login() {
    const [login, setLogin] = useState("");
    const [password, setPassword] = useState("");
    const [loading, setLoading] = useState(false);
    
    const history = useHistory();

    useEffect(() => {
        localStorage.clear();
    }, []);

    //Handle Submit

    async function handleSubmit(event) {
        event.preventDefault();
        try {
            setLoading(true)
            const result = await api.post("/api/v1/users/login", {
                login,
                pass: password,
            });

            if (result.data.status !== false) {
                cardSucess("Login, " + login);

                localStorage.setItem("keT", `${result.data.token}`);
                localStorage.setItem("Session", `${result.data.session}`);
                localStorage.setItem("LoginRouter", true);
                localStorage.setItem("FirstName", `${result.data.name}`);
                localStorage.setItem("LastName", `${result.data.lastName}`);
                localStorage.setItem("upid", result.data.identify);
                setLoading(false);
                history.push("/dashboard");
            } else {
                setLoading(false);
                cardError("Falha no Login!");
                history.push("/");
            }
        } catch (erro) {
            setLoading(false);
            cardError("Falha ao Logar na aplicação!");
        }
    }

    return (
        <>
            <div className="box-aside-left col-md-3"></div>
            <div className="box-aside-left-information col-md-3">
                <h1>Bem vindo !</h1>
                <hr></hr>
                <p>Sistema Portal de Comunicação</p>
            </div>
            <div className="grid-login col-md-12">
                <div className="col col-md-11 box-main-formulario">
                    <div className="image-background">
                        <div className="opacit"></div>
                    </div>

                    <div className="aside-login">
                        <header>
                            <h1>Log In</h1>
                            <hr></hr>
                        </header>

                        <main>
                            <form id="" onSubmit={handleSubmit}>
                                <div className="div-form-input">
                                    <label htmlFor="login">Usuário</label>
                                    <input
                                        type="text"
                                        id="login"
                                        className="form-control inpt"
                                        onChange={(event) =>
                                            setLogin(event.target.value)
                                        }
                                        data-user="login"
                                        placeholder="Digite seu usuário"
                                        autoComplete="off"
                                        maxLength="14"
                                    />
                                </div>
                                <div className="div-form-input">
                                    <label htmlFor="passwd">Senha</label>
                                    <input
                                    
                                        type="password"
                                        id="pass"
                                        className="form-control inpt"
                                        onChange={(event) =>
                                            setPassword(event.target.value)
                                        }
                                        data-pass="password"
                                        placeholder="Digite sua senha"
                                        autoComplete="off"
                                        maxLength="18"
                                    />
                                </div>
                                <Link
                                    style={{ textDecoration: "none" }}
                                    to={`/reset/password`}
                                >
                                    <div className="mt-4">
                                        &rarr; resetar senha ?
                                    </div>
                                </Link>

                                {loading === false  ? <button className="btn btn-outline-dark">
                                    Entrar
                                </button> : 
                                <Button variant="primary" disabled>
                                    <Spinner
                                        animation="grow"
                                        size="sm"
                                        role="status"
                                        aria-hidden="true"
                                    />
                                    Loading...
                                </Button>}
                            </form>
                        </main>
                    </div>
                </div>
            </div>
        </>
    );
}
