import React from 'react';

import Routes from './routes/routes';

import './css/style.css';

import { Provider } from 'react-redux';

import Store from './store/index';

import { ToastContainer } from 'react-toastify';

function App() {
  return (
    <>
        <Provider store={Store}>
            <ToastContainer />
            <Routes />
        </Provider>
    </>
  );
}

export default App;
